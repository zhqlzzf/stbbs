<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config = array (
  'site_name' => '徐州生物工程职业技术学院-校友系统',
  'index_page' => 'index.php',
  'show_captcha' => 'on',
  'site_close' => 'on',
  'site_close_msg' => '网站升级中，暂时关闭。',
  'basic_folder' => '',
  'version' => false,
  'static' => 'white',
  'themes' => 'xiaoyou',
  'logo' => '校友系统',
  'auto_tag' => 'on',
  'encryption_key' => '68b0fe27b883a1c8ad0a2caa7f08cf53',
);
