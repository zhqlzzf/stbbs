<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config = array (
  'comment_order' => 'desc',
  'is_approve' => 'on',
  'per_page_num' => '10',
  'home_page_num' => '20',
  'timespan' => '0',
  'words_limit' => '20000',
);
