<?php
#doc
#	classname:	Student_m
#	scope:		PUBLIC
#	StartBBS起点轻量开源社区系统
#	author :doudou QQ:858292510 startbbs@126.com
#	Copyright (c) 2013 http://www.startbbs.com All rights reserved.
#/doc

class Student_m extends CI_Model
{
	function __construct ()
	{
		parent::__construct();
	}
	
	public function exist($clsid,$truename){
		$query = $this->db->query('SELECT 1 from stb_students  where stu_name='."'".$truename."'".' and c_id='."'".$clsid."'");
		return $query->num_rows();
	}
	
	public function get_students($c_id){
		$this->db->select('username');
		$this->db->from('users');
		$this->db->where('c_id',$c_id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result_array();
		}
	}
}