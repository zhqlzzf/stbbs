<?php
#doc
#	classname:	User_m
#	scope:		PUBLIC
#	StartBBS起点轻量开源社区系统
#	author :doudou QQ:858292510 startbbs@126.com
#	Copyright (c) 2013 http://www.startbbs.com All rights reserved.
#/doc

class Inschoolyear_m extends CI_Model
{
	function __construct ()
	{
		parent::__construct();
	}
	
	public function exist($year){
		$query = $this->db->query('SELECT 1 from stb_inschoolyear  where yid='.$year);
		return $query->num_rows()>0?true:false;
	}
	
	public function save($y_id)
	{
		$data = array(
				'yid' => $y_id
			);
			
		if (!$this->exist($y_id))
		{
			return $this->add($data)?1:-1;
		}
		else 
			return 0;
	}
	public function add($data)
	{			
		if($this->db->insert('inschoolyear',$data))
			return true;
		else
			return false;
	}
	
	public function get_all_classes($page, $limit){
		$this->db->select('*');
		$this->db->from('classes');
		$this->db->limit($limit,$page);
		$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result_array();
		}
	}
	public function get_classes($year,$depid){
		$this->db->select('*');
		$this->db->from('classes');
		$this->db->where('inschoolyear',$year);
		$this->db->where('dep_id',$depid);
		$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result_array();
		}
	}
	
	public function search_class_by_classname($classname)
	{
		$this->db->select('*');
		$this->db->from('classes');
		$this->db->like('c_name',$classname);
		$query = $this->db->get();
		
		return $query->result_array();
	}
	public function get_a_class($c_id){
		$this->db->select('*');
		$this->db->from('classes');
		$this->db->where('c_id', $c_id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result_array();
		}
	}
}