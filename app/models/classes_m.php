<?php
#doc
#	classname:	User_m
#	scope:		PUBLIC
#	StartBBS起点轻量开源社区系统
#	author :doudou QQ:858292510 startbbs@126.com
#	Copyright (c) 2013 http://www.startbbs.com All rights reserved.
#/doc

class Classes_m extends CI_Model
{
	function __construct ()
	{
		parent::__construct();
	}
	public function exist($c_name,$year){
		
		$this->db->select(1);
		$this->db->from('classes');
		$this->db->where('c_name',$c_name);
		$this->db->where('inschoolyear',$year);
		$query=$this->db->get();
		
		return $query->num_rows()>0?true:false;
	}
	public function save($c_id,$c_name,$dep_id,$inschoolyear)
	{
		$data = array(
				'c_id' => $c_id,
				'c_name' => $c_name,
				'dep_id' => $dep_id,
				'inschoolyear'=> $inschoolyear
			);
			
		if (!exist($c_name,$year))
		{
			return add($data);
		}
		return true;
	}
	public function add($data)
	{			
		if($this->db->insert('classes',$data))
			return true;
		else
			return false;
	}
	
	public function get_all_classes($page, $limit){
		$this->db->select('*');
		$this->db->from('classes');
		$this->db->limit($limit,$page);
		$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result_array();
		}
	}
	
	public function get_classes($year,$depid){
		$this->db->select('*');
		$this->db->from('classes');
		$this->db->where('inschoolyear',$year);
		$this->db->where('dep_id',$depid);
		$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result_array();
		}
	}
	
	public function search_class_by_classname($classname)
	{
		$this->db->select('*');
		$this->db->from('classes');
		$this->db->like('c_name',$classname);
		$query = $this->db->get();
		
		return $query->result_array();
	}
	public function get_a_class($c_id){
		$this->db->select('*');
		$this->db->from('classes');
		$this->db->where('c_id', $c_id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result_array();
		}
	}
}