<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#doc
#	classname:	Home
#	scope:		PUBLIC
#	StartBBS起点轻量开源社区系统
#	author :doudou QQ:858292510 startbbs@126.com
#	Copyright (c) 2013 http://www.startbbs.com All rights reserved.
#/doc

class Home extends SB_Controller
{
	function __construct ()
	{
		parent::__construct();

		$this->load->model('topic_m');
		$this->load->model('user_m');
		$this->load->model('cate_m');
		$this->load->library('myclass');
		$this->load->model('link_m');
		$this->home_page_num=($this->config->item('home_page_num'))?$this->config->item('home_page_num'):20;
		
	}
	public function index()
	{
		//获取最新学生列表
		$data['lastreguserslist']= $this->user_m->get_laststudents(6);		
		
		//获取首页大图，可以指定图片数量zhqlzzf9:11 2016/3/18
		$data['bigpiclist']= $this->get_homepage_bigpiclist(5);
		
		//获取首页图片新闻列表，可以指定图片数量zhqlzzf9:11 2016/3/18
		$data['newspiclist']= $this->get_topic_piclist(20);
		
		//获取列表，从toplics表中取
		$data['topic_list'] = $this->topic_m->get_topics_list_nopage($this->home_page_num);
		//从nodes表中取
		$data['catelist'] =$this->cate_m->get_all_cates();
		//echo var_dump($data['catelist']);

		$this->db->cache_on();
		$stats=$this->db->get('site_stats')->result_array();
		$data['stats']=array_column($stats, 'value', 'item');
		//$data['stats']['last_uid']这种写法可以比较方便的去到stats里的属性
		$data['last_user']=$this->db->select('username')->where('uid',@$data['stats']['last_uid'])->get('users')->row_array();
		$data['stats']['last_username']=@$data['last_user']['username'];
		$this->db->cache_off();

		//links
		$data['links']=$this->link_m->get_latest_links();

		//action
		$data['action'] = 'home';
		
		//班级id
		$cr=$this->db->select('c_id')->where('uid',$this->session->userdata('uid'))->get('users')->row_array();
		$data['c_id']= isset($cr['c_id'])?$cr['c_id']:'';
		
		$this->load->view('home',$data);

	}
	public function latest()
	{
		$data['list'] = $this->topic_m->get_topics_list_nopage(5);
		$this->load->view('latest',$data);
	}
	public function search()
	{
		$data['q'] = $this->input->get('q', TRUE);
		$data['title'] = '搜索';
		$this->load->view('search',$data);
	}

	public function getmore ($page=1)
	{
		//分页
		$limit = $this->home_page_num;
		$config['uri_segment'] = 3;
		$config['use_page_numbers'] = TRUE;
		$config['base_url'] = site_url('home/getmore/'.$page);
		$config['total_rows'] = $this->topic_m->count_topics(0);
		$config['per_page'] = $limit;
		$config['first_link'] ='首页';
		$config['last_link'] ='尾页';
		$config['num_links'] = 10;
		
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		
		$start = ($page-1)*$limit;
		$data['pagination'] = $this->pagination->create_links();

		//获取列表
		$data['topic_list'] = $this->topic_m->get_topics_list($start, $limit, 0);
		
		//$data['category'] = $this->cate_m->get_category_by_node_id($node_id);
		$this->load->view('getmore', $data);
	}
	//获取首页大图相关9:12 2016/3/18
	public function get_pic_url($s)
	{
		preg_match('/<img.+src=\"?(.+\.(jpg|gif|bmp|bnp|png))\"?.+>/i',$s,$match);
		//echo $match[1];
		return $match[1];
	}
	//获取首页大图相关9:13 2016/3/18
	public function get_homepage_bigpiclist($counts=5)
	{
		$piclist=array();
		
		$query= $this->topic_m->get_topics_list(0,$counts,20);
		
		//echo sizeof($query);
		for ($i=0;$i<sizeof($query);$i++ )
		{
			$piclist[$i]= $this->get_pic_url($query[$i]['content']);
		}
		return $piclist;
	}
	
	//获取首页新闻图片相关9:13 2016/3/18
	public function get_topic_piclist($counts=5)
	{
		$piclist=array();
		
		$query= $this->topic_m->get_latest_pic_topics($counts);
		
		//echo sizeof($query);
		
		for ($i=0;$i<sizeof($query);$i++ )
		{
			$piclist[$i]['tid']=$query[$i]['topic_id'];
			$piclist[$i]['url']=$this->get_pic_url($query[$i]['content']);
		}
		return $piclist;
	}
	
}