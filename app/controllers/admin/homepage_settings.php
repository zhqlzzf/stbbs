<?php
class Homepage_settings extends Admin_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('cate_m');
		$this->load->model('topic_m');
		$this->load->library('myclass');
		/** 检查登陆 */
		if(!$this->auth->is_admin())
		{
			show_message('非管理员或未登录',site_url('admin/login/do_login'));
		}
	}

	public function index($page=1)
	{
		$data['title'] = '前台首页设置';
		//基本设置
		if($_POST && @$_GET['a']=='bigpic'){
			
			//show_message('话题设定更新成功',site_url('admin/homepage_settings'),1);
		}

		//话题设定
		if($_POST && @$_GET['a']=='bottompic'){
			
		}
		
		//分页
		$limit = 20;
		$config['uri_segment'] = 4;
		$config['use_page_numbers'] = TRUE;
		$config['base_url'] = site_url('admin/topics/index/');
		$config['total_rows'] = $this->db->count_all('topics');
		$config['per_page'] = $limit;
		$config['prev_link'] = '&larr;';
		$config['prev_tag_open'] = '<li class=\'prev\'>';
		$config['prev_tag_close'] = '</li';
		$config['cur_tag_open'] = '<li class=\'active\'><span>';
		$config['cur_tag_close'] = '</span></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_link'] = '&rarr;';
		$config['next_tag_open'] = '<li class=\'next\'>';
		$config['next_tag_close'] = '</li>';
        $config['first_link'] = '首页';
		$config['first_tag_open'] = '<li class=\'first\'>';
		$config['first_tag_close'] = '</li>';
        $config['last_link'] = '尾页';
		$config['last_tag_open'] = '<li class=\'last\'>';
		$config['last_tag_close'] = '</li>';
		$config['num_links'] = 10;
		
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		
		$start = ($page-1)*$limit;
		$data['pagination'] = $this->pagination->create_links();
		$data['topics'] = $this->topic_m->get_topics_list(0,$limit,20);
		
		$data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf_token'] = $this->security->get_csrf_hash();
		$this->load->view('homepage_settings', $data);
	}

	
}