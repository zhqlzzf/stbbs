<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Classes extends Admin_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('classes_m');
		/** 检查登陆 */
		if(!$this->auth->is_admin())
		{
			show_message('非管理员或未登录',site_url('admin/login/do_login'));
		}
	}

	public function index ($page=1)
	{
		$data['title'] = '班级管理';
		$data['act']=$this->uri->segment(3);
		//分页
		$limit = 10;
		$config['uri_segment'] = 4;
		$config['use_page_numbers'] = TRUE;
		$config['base_url'] = site_url('admin/classes/index');
		$config['total_rows'] = $this->db->count_all('classes');
		$config['per_page'] = $limit;
		$config['prev_link'] = '&larr;';
		$config['first_link'] ='首页';
		$config['last_link'] ='尾页';
		$config['prev_tag_open'] = '<li class=\'prev\'>';
		$config['prev_tag_close'] = '</li';
		$config['cur_tag_open'] = '<li class=\'active\'><span>';
		$config['cur_tag_close'] = '</span></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_link'] = '&rarr;';
		$config['next_tag_open'] = '<li class=\'next\'>';
		$config['next_tag_close'] = '</li>';
        $config['last_link'] = '尾页';
		$config['last_tag_open'] = '<li class=\'last\'>';
		$config['last_tag_close'] = '</li>';
		$config['num_links'] = 10;
		
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		
		$start = ($page-1)*$limit;
		$data['pagination'] = $this->pagination->create_links();
		
		$data['classes'] = $this->classes_m->get_all_classes($start, $limit);
		
		$data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf_token'] = $this->security->get_csrf_hash();
		$this->load->view('classes', $data);
		
	}
	public function search()
	{
		//查找用户
		$data['title'] = '班级搜索';
		$data['act']=$this->uri->segment(3);
		if($_POST){
			$data['classes']=$this->classes_m->search_class_by_classname($this->input->post('username'));
		}
		$this->load->view('classes', $data);
	}	
}