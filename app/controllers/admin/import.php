<?php
class Import extends Admin_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('cate_m');
		$this->load->model('topic_m');
		$this->load->model('inschoolyear_m');
		$this->load->library('myclass');
		
		
		/** 检查登陆 */
		if(!$this->auth->is_admin())
		{
			show_message('非管理员或未登录',site_url('admin/login/do_login'));
		}
	}

	public function index($page=1)
	{
		$data['title'] = '前台首页设置';
		//基本设置
		if($_POST && @$_GET['a']=='bigpic'){
			
			//show_message('话题设定更新成功',site_url('admin/homepage_settings'),1);
		}

		//话题设定
		if($_POST && @$_GET['a']=='bottompic'){
			
		}
		
		//分页
		$limit = 20;
		$config['uri_segment'] = 4;
		$config['use_page_numbers'] = TRUE;
		$config['base_url'] = site_url('admin/topics/index/');
		$config['total_rows'] = $this->db->count_all('topics');
		$config['per_page'] = $limit;
		$config['prev_link'] = '&larr;';
		$config['prev_tag_open'] = '<li class=\'prev\'>';
		$config['prev_tag_close'] = '</li';
		$config['cur_tag_open'] = '<li class=\'active\'><span>';
		$config['cur_tag_close'] = '</span></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_link'] = '&rarr;';
		$config['next_tag_open'] = '<li class=\'next\'>';
		$config['next_tag_close'] = '</li>';
        $config['first_link'] = '首页';
		$config['first_tag_open'] = '<li class=\'first\'>';
		$config['first_tag_close'] = '</li>';
        $config['last_link'] = '尾页';
		$config['last_tag_open'] = '<li class=\'last\'>';
		$config['last_tag_close'] = '</li>';
		$config['num_links'] = 10;
		
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		
		$start = ($page-1)*$limit;
		$data['pagination'] = $this->pagination->create_links();
		$data['topics'] = $this->topic_m->get_topics_list(0,$limit,20);
		
		$data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf_token'] = $this->security->get_csrf_hash();
		$this->load->view('import', $data);
	}
	//由于使用了ajax方法，这个方法暂时不用
	private function xls_upload()
    {
        $config['upload_path'] = './uploads/xls';
        $config['allowed_types'] = 'xls';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = '5120';

        $this->load->library('upload', $config);

		$data['msg']='';
        if (!$this->upload->do_upload('xls_file'))
        {
            //$this->avatar($this->upload->display_errors());
			echo $this->upload->display_errors();
			//$data['msg']=$this->upload->display_errors();
        }
        else
        {
            //upload sucess
            //$img_array = $this->upload->data();
			echo $this->upload->file_name;
            //redirect('admin/import','index');    
        }
    }
	
	public function importdata()
	{
		function checkdata($excelData)
		{
			if ($excelData[1][0]==urlencode('姓名'))
				return true;
			else 
				return false;
		}
		
		
		$xlsfile = urldecode($_POST['xlsfile']);
		
		//if xlsfile is null ,no resion to continue
		if (!file_exists($xlsfile))
			die($xlsfile);
		
		if(!$this->auth->is_login())
		{
			die('no  access');
		}
	
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		
		$objReader = IOFactory::createReader('Excel5');//use excel2007 for 2007 format
		
		$objPHPExcel = $objReader->load($xlsfile);
		$objWorksheet = $objPHPExcel->getActiveSheet();
        $highestRow = $objWorksheet->getHighestRow();	
		$highestColumn = $objWorksheet->getHighestColumn();
		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        $excelData = array();
		
        for($row = 1; $row <= $highestRow; $row++) {
            for ($col = 0; $col < $highestColumnIndex; $col++) {
                $excelData[$row][]=(string)$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
            }
        }
		//取入学年份
		$inschoolyear=array();
		for($row = 2; $row <= $highestRow; $row++) {
			array_push($inschoolyear,(string)$objWorksheet->getCellByColumnAndRow(4, $row)->getValue());
		}
		$inschoolyear=array_values(array_unique($inschoolyear));
	
		//$data['excelData'] = $inschoolyear;
		
		
		foreach ($inschoolyear as $v )
		{
			//die($v);
			$code=$this->inschoolyear_m->save($v);			
		}
		
		
		//取专业和班级
		$classes=array();		
		for($row = 2; $row <= $highestRow; $row++) {
			$a=(string)$objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
			$b=(string)$objWorksheet->getCellByColumnAndRow(3, $row)->getValue();
			array_push($classes,trim($a)!=''?$a:$b);
		}
		$classes=array_values(array_unique($classes));
	
		exit(json_encode($classes));
	
		if (checkdata($excelData))
		{
			$data['excelData'] = $excelData;	
			$data['status'] = 'success';
			$data['msg']  = 'ok';		
			exit(json_encode($data));
		}
		else
		{
			die('bu yi zhi'.$excelData[1][0]);
		}		
	}
	
	
}