<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Classes extends SB_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('classes_m');
		$this->load->model('topic_m');
		$this->load->model('cate_m');
		$this->load->library('myclass');
		/** 检查登陆 */
		if(!$this->auth->is_login())
		{
			show_message('非管理员或未登录',site_url('admin/login/do_login'));
		}
	}

	public function index ($page=1)
	{
		$data['title'] = '班级管理';
		$data['act']=$this->uri->segment(3);
		//分页
		$limit = 10;
		$config['uri_segment'] = 4;
		$config['use_page_numbers'] = TRUE;
		$config['base_url'] = site_url('admin/users/index');
		$config['total_rows'] = $this->db->count_all('users');
		$config['per_page'] = $limit;
		$config['prev_link'] = '&larr;';
		$config['first_link'] ='首页';
		$config['last_link'] ='尾页';
		$config['prev_tag_open'] = '<li class=\'prev\'>';
		$config['prev_tag_close'] = '</li';
		$config['cur_tag_open'] = '<li class=\'active\'><span>';
		$config['cur_tag_close'] = '</span></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_link'] = '&rarr;';
		$config['next_tag_open'] = '<li class=\'next\'>';
		$config['next_tag_close'] = '</li>';
        $config['last_link'] = '尾页';
		$config['last_tag_open'] = '<li class=\'last\'>';
		$config['last_tag_close'] = '</li>';
		$config['num_links'] = 10;
		
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		
		$start = ($page-1)*$limit;
		$data['pagination'] = $this->pagination->create_links();
		
		$data['classes'] = $this->classes_m->get_all_classes($start, $limit);
		
		$data['csrf_name'] = $this->security->get_csrf_token_name();
        $data['csrf_token'] = $this->security->get_csrf_hash();
		$this->load->view('classes', $data);
		
	}
	public function show ($topic_id=1,$page=1)
	{
		$content = $this->topic_m->get_topic_by_topic_id($topic_id);
		if(!$content){
			//show_message('贴子不存在',site_url('/'));
			//新建一个班级
			$this->add($topic_id);
		} else
		{
			$content = $this->topic_m->get_topic_by_topic_id($topic_id);
			$content['content']=stripslashes($content['content']);
			$data['content']=$content;		
			//更新浏览数
			$this->db->where('topic_id',$content['topic_id'])->update('topics',array('views'=>$content['views']+1));
			
			//获取当前分类
			$data['cate']=$this->db->get_where('nodes',array('node_id'=>$content['node_id']))->row_array();

			//上下主题
			$data['content']['previous'] = $this->topic_m->get_near_id($topic_id,$data['cate']['node_id'],0);
			$data['content']['next'] = $this->topic_m->get_near_id($topic_id,$data['cate']['node_id'],1);
			$data['content']['previous']=$data['content']['previous']['topic_id'];
			$data['content']['next']=$data['content']['next']['topic_id'];
			//描述
			$data['content']['description']= '';

			//取已经加入的同学的列表
			$this->load->model('student_m');
			$data['InStudents']=$this->student_m->get_students($topic_id);
			
			$this->load->view('class_show', $data);
		}
	}

	public function add($c_id)
	{
		//加载form类，为调用错误函数,需view前加载
		$this->load->helper('form');
		//获取已选择过的分类名称
		$node_id=22;
		$data['cate']=$this->db->get_where('nodes',array('node_id'=>$node_id))->row_array();
		
		$data['title'] = '发表话题';
		$uid = $this->session->userdata('uid');
		$this->load->model ('user_m');
		$user = $this->user_m->get_user_by_uid($uid);
		if(!$this->auth->is_login()) {
			redirect('user/login/');
		}
		if(!$this->auth->user_permit($node_id)) {//权限
			$this->session->set_flashdata('error', '您无权在此节点发表话题!请重新选择节点');
			redirect('topic/add/');//加入此行代码，否则跳转不回来。zhqlzzf 20160317
			exit;
		}
		
		$class=$this->classes_m->get_a_class($c_id);
		$data = array(
				'topic_id' => $c_id,
				'title' => $class[0]['c_name'],
				'content' => '请点击下方的编辑按钮，然后插入相应班级照片',
				'node_id' => $node_id,
				'uid' => $uid,
				'addtime' => time(),
				'updatetime' => time(),
				'lastreply' => time(),
				'views' => 0,
				'ord'=>time()
			);
		$this->load->helper('format_content');
		$data['content']=format_content($data['content']);
		$data['is_hidden'] = 0;	
		$this->topic_m->add($data);
		
		$this->show($c_id,1);	
	}

	public function search()
	{
		//查找用户
		$data['title'] = '班级搜索';
		$data['act']=$this->uri->segment(3);
		if($_POST){
			$data['classes']=$this->classes_m->search_class_by_classname($this->input->post('username'));
		}
		$this->load->view('classes', $data);
	}
}