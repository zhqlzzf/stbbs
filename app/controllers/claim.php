<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#doc
#	classname:	User
#	scope:		PUBLIC
#	StartBBS起点轻量开源社区系统
#	author :doudou QQ:858292510 startbbs@126.com
#	Copyright (c) 2013 http://www.startbbs.com All rights reserved.
#/doc

class Claim extends SB_Controller
{
	function __construct ()
	{
		parent::__construct();
		$this->load->model ('classes_m');
		$this->load->model ('student_m');
		$this->load->library('form_validation');
	}
	public function doclaim()
	{		
		if($_POST && $this->form_validation->run() === TRUE){			
			$c_id = $this->input->post('myClass',true);
			$myName= $this->input->post('truename',true);
			$phone=$this->input->post('phone',true);
			$email=$this->input->post('email',true);
			
			//检查校友数据库，如果在校友数据库里没有，退出
			if ($this->student_m->exist($c_id,$myName)<1)
			{	
				redirect();		
				return;
			}
			
			//检查stb_users，如果存在,直接跳转到登陆
			$this->load->model ('user_m');
			if ($this->user_m->exist($c_id,$myName)>0)
			{	
				redirect('user/login');
				return;
			}
			
			
			$password = $this->input->post('password',true);
			$salt =get_salt();
			
			$data = array(
				'username' => strip_tags($this->input->post('truename')),
				'email' => $this->input->post('email',true),
				'password' => password_dohash($password,$salt),
				'salt' => $salt,
				'credit' => $this->config->item('credit_start'),
				'ip' => get_onlineip(),
				'group_type' => 2,//通过认领操作注册的用户，直接为2类型
				'gid' => 3,
				'regtime' => time(),
				'is_active' => 1,
				'c_id' => $c_id,
				'phone'=>$phone,
				'email'=>$email
			);
			if($this->user_m->register($data)){
				$uid = $this->db->insert_id();
				$newdata=array('username'=>$data['username'],'password'=>$password);
				$this->user_m->login($newdata);
				//发送注册邮件
				if($this->config->item('mail_reg')=='on'){
					$subject='欢迎加入'.$this->config->item('site_name');
					$message='欢迎来到 '.$this->config->item('site_name').' 论坛<br/>请妥善保管这封信件。您的帐户信息如下所示：<br/>----------------------------<br/>用户名：'.$data['username'].'<br/>论坛链接: '.site_url().'<br/>----------------------------<br/><br/>感谢您的注册！<br/><br/>-- <br/>'.$this->config->item('site_name');
					send_mail($data['email'],$subject,$message);
					//echo $this->email->print_debugger();
				}
				$this->db->set('value',$uid,false)->where('item','last_uid')->update('site_stats');
				$this->db->set('value','value+1',false)->where('item','total_users')->update('site_stats');
				redirect();
			}
		}
		else{
			$data['csrf_name'] = $this->security->get_csrf_token_name();
            $data['csrf_token'] = $this->security->get_csrf_hash();
			
			//查询院系数据，放到data里
			$this->load->database();
			$query=$this->db->query('select dep_id, dep_name from stb_departments ');
			$yx= array();
			$yx['请选择']='请选择';
			foreach($query->result() as $row)
			{				
				$yx[$row->dep_id]=$row->dep_name;
			}
			$data['yx']=$yx;
			
			//查询年份数据，放到data里
			$yearquery=$this->db->query('select yid from stb_inSchoolyear ');
			$year= array();
			$year['请选择']='请选择';
			foreach($yearquery->result() as $rowyear)
			{				
				$year[$rowyear->yid]=$rowyear->yid;
			}
			$data['year']=$year;
			
			$this->load->view('claim',$data);
		}
	}
		
	public function index()
	{
		//加载form类，为调用错误函数,需view前加载
		$this->load->helper('form');

		$data['title'] = '认领用户';
		if ($this->auth->is_login()) {
			show_message('已登录，请退出再进行认领操作',site_url());
		}
		if($_POST && $this->form_validation->run() === TRUE){
			
		} 
		else{
            $data['csrf_name'] = $this->security->get_csrf_token_name();
            $data['csrf_token'] = $this->security->get_csrf_hash();
			
			//查询院系数据，放到data里
			$this->load->database();
			$query=$this->db->query('select dep_id, dep_name from stb_departments ');
			$yx= array();
			$yx['请选择']='请选择';
			foreach($query->result() as $row)
			{				
				$yx[$row->dep_id]=$row->dep_name;
			}
			$data['yx']=$yx;
			
			//查询年份数据，放到data里
			$yearquery=$this->db->query('select yid from stb_inSchoolyear ');
			$year= array();
			$year['请选择']='请选择';
			foreach($yearquery->result() as $rowyear)
			{				
				$year[$rowyear->yid]=$rowyear->yid;
			}
			$data['year']=$year;
			//print_r($this->get_classes('2014','cjxx'));
			$this->load->view('claim',$data);
		}
	} 
	/*
	总结一下php json的写法：
	1：在model层进行数据的获取，使用return $query->result_array();进行返回，为什么要用result_array？因为这样就是一个数组了，下面就好处理了，比如后面可以使用count()来获取行数。
	2：进入control层，要设置输出方式，就是set_header为JSON,这样才能返回类型为json，
	3：在 control最后进行返回到前台时,要使用echo的方式返回，不要使用return的方式返回。
	4：通过上面几部的操作，前台应该可以得到json数据了。下面的处理参见claim.js里的第27行的写法。ok
	*/
    public function get_classes(){
		$this->load->helper('json');
		
		$year=$this->input->post ('inschoolyear');
		$depid= $this->input->post ('dep_id');
		$res_arr= $this->classes_m->get_classes($year,$depid);//返回的是$query->result_array() ;
		
		//这里很重要，要设置输出方式
		$this->output->set_header('Content-Type: application/json; charset=utf-8');
		
		$result= array();
		if (null!==$res_arr)
		{
			for ($i=0;$i<count($res_arr);$i++)
			{
				$result[$i]['id']=$res_arr[$i]['c_id'];
				$result[$i]['name']=$res_arr[$i]['c_name'];
			}				
		}
		else
		{
			$result[0]['id']='error';
			$result[0]['name']='error';			
		}
		//这里不能用return要用echo，很重要
		echo json_encode($result);
	}
	
	public function exist(){
		
		
		$c_id=$this->input->post ('c_id');
		$stu_name= $this->input->post ('stu_name');
		$res= $this->student_m->exist($c_id,$stu_name);
		
		if ($res>0)
		{
			echo 1;				
		}
		else
		{
			echo 0;		
		}
		
	}
}