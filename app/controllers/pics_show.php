<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#doc
#	classname:	topic
#	scope:		PUBLIC
#	StartBBS起点轻量开源社区系统
#	author :doudou QQ:858292510 startbbs@126.com
#	Copyright (c) 2013 http://www.startbbs.com All rights reserved.
#/doc

class pics_show extends SB_controller
{

	function __construct ()
	{
		parent::__construct();
		$this->load->model('topic_m');
		$this->load->model('cate_m');
		$this->load->library('myclass');
		$this->load->library('form_validation');
	}
	public function  index()
	{
		//获取首页图片新闻列表，可以指定图片数量zhqlzzf9:11 2016/3/18
		$data['newspiclist']= $this->get_topic_piclist(1000);
		$this->load->view('pics_show',$data);
	}
	
	//获取首页新闻图片相关9:13 2016/3/18
	public function get_topic_piclist($counts)
	{
		$piclist=array();
		
		$query= $this->topic_m->get_latest_pic_topics($counts);
		
		for ($i=0;$i<sizeof($query);$i++ )
		{
			$piclist[$i]['tid']=$query[$i]['topic_id'];
			$piclist[$i]['url']=$this->get_pic_url($query[$i]['content']);
		}
		return $piclist;
	}
	
	//获取首页大图相关9:12 2016/3/18
	public function get_pic_url($s)
	{
		preg_match('/<img.+src=\"?(.+\.(jpg|gif|bmp|bnp|png))\"?.+>/i',$s,$match);
		//echo $match[1];
		return $match[1];
	}
	
	/*
	 *从content里取指定数量的pic，用于游客访问时显示部分内容的效果。
	 *参数：str:待处理的字符串,count:要显示图片的数量
	*/
	public function getPic($str,$count)
	{
		$result="";	
		$i=0;
		preg_match_all('/<\s*img\s+[^>]*?src\s*=\s*(\'|\")(.*?)\\1[^>]*?\/?\s*>/i',$str,$match);
		foreach($match[0] as $val)
		{
			if ($i<$count)
				$result=$result.$val;
			$i++;
		}			
		return @$result;
	}	
}