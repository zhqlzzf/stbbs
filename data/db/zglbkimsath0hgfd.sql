#
# TABLE STRUCTURE FOR: stb_comments
#

DROP TABLE IF EXISTS stb_comments;

CREATE TABLE `stb_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL DEFAULT '0',
  `uid` int(11) NOT NULL DEFAULT '0',
  `content` text,
  `replytime` char(10) DEFAULT NULL,
  PRIMARY KEY (`id`,`topic_id`,`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: stb_favorites
#

DROP TABLE IF EXISTS stb_favorites;

CREATE TABLE `stb_favorites` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `favorites` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `content` mediumtext NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: stb_links
#

DROP TABLE IF EXISTS stb_links;

CREATE TABLE `stb_links` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `logo` varchar(200) DEFAULT NULL,
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO stb_links (`id`, `name`, `url`, `logo`, `is_hidden`) VALUES (1, 'StartBBS', 'http://www.startbbs.com', '', 1);
INSERT INTO stb_links (`id`, `name`, `url`, `logo`, `is_hidden`) VALUES (2, '徐州生物工程职业技术学院', 'http://www.xzsw.net', NULL, 0);
INSERT INTO stb_links (`id`, `name`, `url`, `logo`, `is_hidden`) VALUES (3, '矿大', 'http://www.cumt.edu.cn', NULL, 0);


#
# TABLE STRUCTURE FOR: stb_message
#

DROP TABLE IF EXISTS stb_message;

CREATE TABLE `stb_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dialog_id` int(11) NOT NULL,
  `sender_uid` int(11) NOT NULL,
  `receiver_uid` int(11) NOT NULL,
  `content` text NOT NULL,
  `create_time` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dialog_id` (`dialog_id`),
  KEY `sender_uid` (`sender_uid`),
  KEY `create_time` (`create_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: stb_message_dialog
#

DROP TABLE IF EXISTS stb_message_dialog;

CREATE TABLE `stb_message_dialog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_uid` int(11) NOT NULL,
  `receiver_uid` int(11) NOT NULL,
  `last_content` text NOT NULL,
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  `sender_remove` tinyint(1) NOT NULL DEFAULT '0',
  `receiver_remove` tinyint(1) NOT NULL DEFAULT '0',
  `sender_read` tinyint(1) NOT NULL DEFAULT '1',
  `receiver_read` tinyint(1) NOT NULL DEFAULT '0',
  `messages` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `uid` (`sender_uid`,`receiver_uid`),
  KEY `update_time` (`update_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: stb_nodes
#

DROP TABLE IF EXISTS stb_nodes;

CREATE TABLE `stb_nodes` (
  `node_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `pid` smallint(5) NOT NULL DEFAULT '0',
  `sortid` int(11) NOT NULL DEFAULT '0',
  `cname` varchar(30) DEFAULT NULL COMMENT '分类名称',
  `content` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `ico` varchar(128) NOT NULL DEFAULT 'uploads/ico/default.png',
  `master` varchar(100) NOT NULL,
  `permit` varchar(255) DEFAULT NULL,
  `listnum` mediumint(8) unsigned DEFAULT '0',
  `clevel` varchar(25) DEFAULT NULL,
  `cord` smallint(6) DEFAULT NULL,
  `node_type` varchar(10) NOT NULL DEFAULT 'normal',
  PRIMARY KEY (`node_id`,`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`) VALUES (1, 0, 1, '测试', '', '', 'uploads/ico/default.png', '', NULL, 0, NULL, NULL, 'normal');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`) VALUES (2, 1, 0, '信息', '', '', 'uploads/ico/default.png', '', NULL, 1, NULL, NULL, 'normal');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`) VALUES (3, 0, 2, '校庆专栏', '', '', 'uploads/ico/default.png', '', NULL, 0, NULL, NULL, 'normal');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`) VALUES (4, 3, 0, '筹备', '', '', 'uploads/ico/default.png', '', NULL, 0, NULL, NULL, 'normal');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`) VALUES (7, 3, 0, '现场', '', '', 'uploads/ico/default.png', '', NULL, 0, NULL, NULL, 'normal');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`) VALUES (11, 0, 1, '我的母校', '', '', 'uploads/ico/default.png', '', NULL, 2, NULL, NULL, 'normal');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`) VALUES (12, 11, 0, '相册', '', '', 'uploads/ico/default.png', '', NULL, 0, NULL, NULL, 'normal');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`) VALUES (13, 11, 0, '校园', '', '', 'uploads/ico/default.png', '', NULL, 0, NULL, NULL, 'normal');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`) VALUES (14, 0, 0, '校友工作动态', '', '', 'uploads/ico/default.png', '', NULL, 3, NULL, NULL, 'normal');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`) VALUES (15, 0, 1, '通知公告', '', '', 'uploads/ico/default.png', '', NULL, 3, NULL, NULL, 'normal');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`) VALUES (16, 0, 0, '校友风采', '', '', 'uploads/ico/default.png', '', NULL, 1, NULL, NULL, 'normal');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`) VALUES (17, 0, 0, '校友捐赠', '', '', 'uploads/ico/default.png', '', NULL, 0, NULL, NULL, 'normal');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`) VALUES (18, 0, 0, '校友通讯', '', '', 'uploads/ico/default.png', '', NULL, 1, NULL, NULL, 'normal');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`) VALUES (19, 0, 0, '固定栏目', '', '', 'uploads/ico/default.png', '', '1,2', 0, NULL, NULL, 'normal');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`) VALUES (20, 19, 0, '首页大图', '', '', 'uploads/ico/default.png', '', '1,6', 7, NULL, NULL, 'bigPic');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`) VALUES (21, 19, 0, '班级相册', '', '', 'uploads/ico/default.png', '', '1,2,4', 0, NULL, NULL, 'bigPic');


#
# TABLE STRUCTURE FOR: stb_notifications
#

DROP TABLE IF EXISTS stb_notifications;

CREATE TABLE `stb_notifications` (
  `nid` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) DEFAULT NULL,
  `suid` int(11) DEFAULT NULL,
  `nuid` int(11) NOT NULL DEFAULT '0',
  `ntype` tinyint(1) DEFAULT NULL,
  `ntime` int(10) DEFAULT NULL,
  PRIMARY KEY (`nid`,`nuid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: stb_page
#

DROP TABLE IF EXISTS stb_page;

CREATE TABLE `stb_page` (
  `pid` tinyint(6) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  `go_url` varchar(100) DEFAULT NULL,
  `add_time` int(10) DEFAULT NULL,
  `is_hidden` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO stb_page (`pid`, `title`, `content`, `go_url`, `add_time`, `is_hidden`) VALUES (1, '单页面测试', '单页面测试单页面测试单页面测试单页面测试单页面测试', '', 1458100596, 0);


#
# TABLE STRUCTURE FOR: stb_settings
#

DROP TABLE IF EXISTS stb_settings;

CREATE TABLE `stb_settings` (
  `id` tinyint(5) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `type` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`title`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (1, 'site_name', '徐州生物工程职业技术学院-校友系统', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (2, 'welcome_tip', '欢迎回到母校', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (3, 'short_intro', '2016年是我校建校60周年', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (4, 'show_captcha', 'on', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (5, 'site_run', '0', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (6, 'site_stats', '统计代码																																																																																																																																																										', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (7, 'site_keywords', '徐州生物工程职业技术学院,校友系统,校庆', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (8, 'site_description', '徐州生物工程职业技术学院,校友系统,校庆', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (9, 'money_title', '银币', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (10, 'per_page_num', '0', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (11, 'is_rewrite', 'off', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (12, 'show_editor', 'on', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (13, 'comment_order', '0', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (14, 'bigPicCounts', '5', 0);


#
# TABLE STRUCTURE FOR: stb_site_stats
#

DROP TABLE IF EXISTS stb_site_stats;

CREATE TABLE `stb_site_stats` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `item` varchar(20) NOT NULL,
  `value` int(10) DEFAULT '0',
  `update_time` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

INSERT INTO stb_site_stats (`id`, `item`, `value`, `update_time`) VALUES (1, 'last_uid', 10, NULL);
INSERT INTO stb_site_stats (`id`, `item`, `value`, `update_time`) VALUES (2, 'total_users', 9, NULL);
INSERT INTO stb_site_stats (`id`, `item`, `value`, `update_time`) VALUES (3, 'today_topics', 1, 1466735749);
INSERT INTO stb_site_stats (`id`, `item`, `value`, `update_time`) VALUES (4, 'yesterday_topics', 1, 1466735749);
INSERT INTO stb_site_stats (`id`, `item`, `value`, `update_time`) VALUES (5, 'total_topics', 18, NULL);
INSERT INTO stb_site_stats (`id`, `item`, `value`, `update_time`) VALUES (6, 'total_comments', 3, NULL);
INSERT INTO stb_site_stats (`id`, `item`, `value`, `update_time`) VALUES (7, 'total_nodes', 0, NULL);
INSERT INTO stb_site_stats (`id`, `item`, `value`, `update_time`) VALUES (8, 'total_tags', 0, NULL);


#
# TABLE STRUCTURE FOR: stb_tags
#

DROP TABLE IF EXISTS stb_tags;

CREATE TABLE `stb_tags` (
  `tag_id` int(10) NOT NULL AUTO_INCREMENT,
  `tag_title` varchar(30) NOT NULL,
  `topics` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tag_id`),
  UNIQUE KEY `tag_title` (`tag_title`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

INSERT INTO stb_tags (`tag_id`, `tag_title`, `topics`) VALUES (1, '座谈会', 1);
INSERT INTO stb_tags (`tag_id`, `tag_title`, `topics`) VALUES (2, '事业', 1);
INSERT INTO stb_tags (`tag_id`, `tag_title`, `topics`) VALUES (3, '学校', 1);
INSERT INTO stb_tags (`tag_id`, `tag_title`, `topics`) VALUES (4, '联系方式', 2);
INSERT INTO stb_tags (`tag_id`, `tag_title`, `topics`) VALUES (5, '沟通能力', 2);
INSERT INTO stb_tags (`tag_id`, `tag_title`, `topics`) VALUES (6, '人际交往', 2);
INSERT INTO stb_tags (`tag_id`, `tag_title`, `topics`) VALUES (7, '工作职责', 2);
INSERT INTO stb_tags (`tag_id`, `tag_title`, `topics`) VALUES (8, '毕业生', 2);
INSERT INTO stb_tags (`tag_id`, `tag_title`, `topics`) VALUES (9, '教学楼', 1);
INSERT INTO stb_tags (`tag_id`, `tag_title`, `topics`) VALUES (10, '阿萨德', 1);
INSERT INTO stb_tags (`tag_id`, `tag_title`, `topics`) VALUES (11, 'dfdfd', 1);
INSERT INTO stb_tags (`tag_id`, `tag_title`, `topics`) VALUES (12, '安卓系统', 1);
INSERT INTO stb_tags (`tag_id`, `tag_title`, `topics`) VALUES (13, '沙门氏菌', 1);
INSERT INTO stb_tags (`tag_id`, `tag_title`, `topics`) VALUES (14, '项目立项', 1);
INSERT INTO stb_tags (`tag_id`, `tag_title`, `topics`) VALUES (15, '智能手机', 1);
INSERT INTO stb_tags (`tag_id`, `tag_title`, `topics`) VALUES (16, '注塑模具', 1);
INSERT INTO stb_tags (`tag_id`, `tag_title`, `topics`) VALUES (17, '软件', 1);


#
# TABLE STRUCTURE FOR: stb_tags_relation
#

DROP TABLE IF EXISTS stb_tags_relation;

CREATE TABLE `stb_tags_relation` (
  `tag_id` int(10) NOT NULL DEFAULT '0',
  `topic_id` int(10) DEFAULT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `fid` (`topic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (1, 2);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (2, 2);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (3, 2);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (4, 3);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (5, 3);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (6, 3);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (7, 3);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (8, 3);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (4, 2);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (5, 2);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (6, 2);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (7, 2);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (8, 2);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (9, 3);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (10, 4);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (11, 5);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (12, 12);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (13, 12);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (14, 12);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (15, 12);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (16, 12);
INSERT INTO stb_tags_relation (`tag_id`, `topic_id`) VALUES (17, 15);


#
# TABLE STRUCTURE FOR: stb_topics
#

DROP TABLE IF EXISTS stb_topics;

CREATE TABLE `stb_topics` (
  `topic_id` int(11) NOT NULL AUTO_INCREMENT,
  `node_id` smallint(5) NOT NULL DEFAULT '0',
  `uid` mediumint(8) NOT NULL DEFAULT '0',
  `ruid` mediumint(8) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `content` text,
  `addtime` int(10) DEFAULT NULL,
  `updatetime` int(10) DEFAULT NULL,
  `lastreply` int(10) DEFAULT NULL,
  `views` int(10) DEFAULT '0',
  `comments` smallint(8) DEFAULT '0',
  `favorites` int(10) unsigned DEFAULT '0',
  `closecomment` tinyint(1) DEFAULT NULL,
  `is_top` tinyint(1) NOT NULL DEFAULT '0',
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `ord` int(10) unsigned NOT NULL DEFAULT '0',
  `hasImg` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`topic_id`,`node_id`,`uid`),
  KEY `updatetime` (`updatetime`),
  KEY `ord` (`ord`),
  FULLTEXT KEY `title` (`title`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

INSERT INTO stb_topics (`topic_id`, `node_id`, `uid`, `ruid`, `title`, `keywords`, `content`, `addtime`, `updatetime`, `lastreply`, `views`, `comments`, `favorites`, `closecomment`, `is_top`, `is_hidden`, `ord`, `hasImg`) VALUES (1, 15, 1, NULL, '班级默认首页', '', '班级默认首页班级默认首页班级默认首页', 1458105053, 1458105053, 1458105053, 4, 0, 0, NULL, 0, 0, 1458105053, 0);
INSERT INTO stb_topics (`topic_id`, `node_id`, `uid`, `ruid`, `title`, `keywords`, `content`, `addtime`, `updatetime`, `lastreply`, `views`, `comments`, `favorites`, `closecomment`, `is_top`, `is_hidden`, `ord`, `hasImg`) VALUES (2, 15, 1, NULL, '关于推荐2012届毕业生校友工作联络员的通知', '联系方式,沟通能力,人际交往,工作职责,毕业生', '各学院（所、中心）：<br />\r\n为搭建毕业生与母校沟通的桥梁，加强毕业生走向社会后与所在地校友会的联系，帮助毕业校友尽快适应社会，充分利用校友资源尽快成长成才，同时，及时掌握校友信息，发挥广大校友在母校事业发展中的积极作用，促进母校和校友事业的共同发展，学校拟在2012届毕业生中选拔聘任校友工作联络员，现将有关事项通知如下：<br />\r\n一、校友工作联络员基本要求<br />\r\n1. 热爱学校，关注支持学校的建设发展，富有奉献精神，热心为校友服务；在校期间担任学生干部的同学优先。<br />\r\n2. 在同学中有较高威信，凝聚力强，有较强的责任心和组织协调能力，具有良好的人际交往和沟通能力。<br />\r\n二、校友工作联络员基本工作职责<br />\r\n1. 保持与学校及本班级同学以及当地校友会的联系，掌握同学动态，协助学校健全、更新校友联络信息。及时向学院和校友总会提供本班同学的有效联系方式，以及校友在各行各业取得的杰出业绩和对母校的建议意见等各类信息；每年12月更新一次班级通讯录(模板可在校友网公告栏下载)，并以电子文档形式发给学院毕业班辅导员及学校校友工作办公室。<br />\r\n2. 促进校友与校友、校友与母校间的沟通和联络，及时向校友总会提供校友在各行各业取得的重要业绩，报道校友的优秀事迹。<br />\r\n3. 积极参加校友总会和当地校友分会的活动。积极协助本届本班级校友举行各类校友聚会活动，并及时向校友总会反馈。<br />\r\n4. 收集本班级同学对学校发展的建议，支持学校建设发展。<br />\r\n5. 如因个人原因不能继续担任本班级的校友工作联络员，应及时推荐新的联络员人选，并报学校校友工作办公室。<br />\r\n三、校友工作联络员享有权利<br />\r\n1. 学校颁发校友联络员聘书。<br />\r\n2. 可从校友总会和所在地校友会及时了解掌握母校、校友会和校友的最新情况。<br />\r\n3. 优先参加校友总会和地方校友会组织的各种联谊、培训和交流活动。<br />\r\n4. 可优先经校友总会推荐，成为工作所在地校友分会理事或理事候选人。<br />\r\n5. 赠阅校报、校友通讯等刊物。<br />\r\n四、2012届校友工作联络员选拔程序<br />\r\n1. 2012届毕业生校友工作联络员采用自荐和组织推荐相结合的方式。本专科每个毕业班推荐1-2名联络员；研究生按学院推荐1-3名联络员。请各学院、有关部门抓紧时间做好相关遴选推荐工作，并于6月15日前填写山东师范大学2012届毕业生校友工作联络员信息表(表格可在校友网公告栏下载)，报送至校友工作办公室，电子版发送至校友办邮箱：xiaoyouban@sdnu.edu.cn。<br />\r\n2. 根据学院推荐名单，经审核确认后，由学校统一颁发“山东师范大学2012届毕业生校友工作联络员”聘书。<br />\r\n选拔聘任校友工作联络员是做好校友工作，服务校友、服务学校的基础性工作，各学院、有关部门要高度重视毕业生校友工作，加强对此项工作的指导，推荐符合要求的毕业生担任校友工作', 1458105188, 1458105188, 1458105188, 30, 0, 0, NULL, 0, 0, 1458105188, 0);
INSERT INTO stb_topics (`topic_id`, `node_id`, `uid`, `ruid`, `title`, `keywords`, `content`, `addtime`, `updatetime`, `lastreply`, `views`, `comments`, `favorites`, `closecomment`, `is_top`, `is_hidden`, `ord`, `hasImg`) VALUES (5, 20, 9, NULL, 'dfdfd', 'dfdfd', '<img  src=\"http://localhost/stbbs/uploads/image/201603/20160317092312_88357.jpg\" alt=\"\">', 1458198841, 1458198841, 1458198841, 3, 0, 0, NULL, 0, 0, 1458198841, 1);
INSERT INTO stb_topics (`topic_id`, `node_id`, `uid`, `ruid`, `title`, `keywords`, `content`, `addtime`, `updatetime`, `lastreply`, `views`, `comments`, `favorites`, `closecomment`, `is_top`, `is_hidden`, `ord`, `hasImg`) VALUES (6, 20, 1, NULL, '0101', '', '<img src=\"http://xyw.sdnu.edu.cn/defaults/images/login/2-1.jpg\" alt=\"\">', 1458263332, 1458263332, 1458263332, 1, 0, 0, NULL, 0, 0, 1458263332, 0);
INSERT INTO stb_topics (`topic_id`, `node_id`, `uid`, `ruid`, `title`, `keywords`, `content`, `addtime`, `updatetime`, `lastreply`, `views`, `comments`, `favorites`, `closecomment`, `is_top`, `is_hidden`, `ord`, `hasImg`) VALUES (7, 20, 1, NULL, '0202', '', '<img src=\"http://xyw.sdnu.edu.cn/defaults/images/login/2-2.jpg\" alt=\"\">', 1458263351, 1458263351, 1458263351, 2, 0, 0, NULL, 0, 0, 1458263351, 1);
INSERT INTO stb_topics (`topic_id`, `node_id`, `uid`, `ruid`, `title`, `keywords`, `content`, `addtime`, `updatetime`, `lastreply`, `views`, `comments`, `favorites`, `closecomment`, `is_top`, `is_hidden`, `ord`, `hasImg`) VALUES (8, 20, 1, NULL, '0303', '', '<img src=\"http://xyw.sdnu.edu.cn/defaults/images/login/2-3.jpg\" alt=\"\">', 1458263362, 1458263362, 1458263362, 1, 0, 0, NULL, 0, 0, 1458263362, 0);
INSERT INTO stb_topics (`topic_id`, `node_id`, `uid`, `ruid`, `title`, `keywords`, `content`, `addtime`, `updatetime`, `lastreply`, `views`, `comments`, `favorites`, `closecomment`, `is_top`, `is_hidden`, `ord`, `hasImg`) VALUES (9, 20, 1, NULL, '0404', '', '<img src=\"http://xyw.sdnu.edu.cn/defaults/images/login/2-4.jpg\" alt=\"\">', 1458263375, 1458263375, 1458263375, 1, 0, 0, NULL, 0, 0, 1458263375, 0);
INSERT INTO stb_topics (`topic_id`, `node_id`, `uid`, `ruid`, `title`, `keywords`, `content`, `addtime`, `updatetime`, `lastreply`, `views`, `comments`, `favorites`, `closecomment`, `is_top`, `is_hidden`, `ord`, `hasImg`) VALUES (10, 20, 1, NULL, '0505', '', '<img src=\"http://xyw.sdnu.edu.cn/defaults/images/login/2-5.jpg\" alt=\"\">', 1458263385, 1458263385, 1458263385, 2, 0, 0, NULL, 0, 0, 1458263385, 0);
INSERT INTO stb_topics (`topic_id`, `node_id`, `uid`, `ruid`, `title`, `keywords`, `content`, `addtime`, `updatetime`, `lastreply`, `views`, `comments`, `favorites`, `closecomment`, `is_top`, `is_hidden`, `ord`, `hasImg`) VALUES (11, 20, 9, NULL, '0606', '', '<img src=\"http://www.pku.edu.cn/images/content/2016-03/20160316090614507019.jpg\" alt=\"\">', 1458265251, 1458265251, 1458265251, 3, 0, 0, NULL, 0, 0, 1458265251, 0);
INSERT INTO stb_topics (`topic_id`, `node_id`, `uid`, `ruid`, `title`, `keywords`, `content`, `addtime`, `updatetime`, `lastreply`, `views`, `comments`, `favorites`, `closecomment`, `is_top`, `is_hidden`, `ord`, `hasImg`) VALUES (12, 14, 1, NULL, '我院新一批院级科技计划项目获得立项', '安卓系统,沙门氏菌,项目立项,智能手机,注塑模具', '3月17日，院级科研计划项目立项评审会议在院综合楼一楼会议室召开。经过院学术委员会的严格评审，“4G智能手机微焊点用新型钎料的研制”等8个项目获准立项为新一批院级科技计划项目。<br />\r\n<br />\r\n本次院级科研计划项目立项经过科研团队申报、评委预审、现场汇报与答辩、学术委员会投票等系列环节，确定了2个重点项目和6个一般项目，并分别给予不同经费资助。<br />\r\n为了保证新一批科研计划项目的实施水平，科研处将协同其他相关部门，督促各项目组按照立项评审意见调整研究细节，严格科研经费管理，认真开展项目中期检查与验收鉴定等相关工作，着力培育新一批科研成果，提升我院科研综合实力。<br />\r\n（供稿：科研处）<br />\r\n附院级科技计划项目立项名单。<br />\r\n项目名称<br />\r\n所属系部<br />\r\n主持人<br />\r\n项目类别<br />\r\n优良园林树种引进、繁育与应用研究<br />\r\n农林工程系<br />\r\n高政平<br />\r\n一般项目<br />\r\n黄芪保健酸奶发酵工艺的优化<br />\r\n生物工程系<br />\r\n王丹<br />\r\n一般项目<br />\r\n大型塑料井注塑模具刚强度分析和结构优化设计<br />\r\n机电工程系<br />\r\n师彩云<br />\r\n一般项目<br />\r\n基于安卓系统的学生请销假APP与基于PC端的学生请销假工作流的比较研究<br />\r\n财经信息系<br />\r\n杨晓征<br />\r\n一般项目<br />\r\n徐州地区禽沙门氏菌耐药性检测及防控体系的建立与推广<br />\r\n动物工程系<br />\r\n张林吉<br />\r\n一般项目<br />\r\n铬胁迫下小麦的生理响应及调控措施研究<br />\r\n农林工程系<br />\r\n曹丹<br />\r\n一般项目<br />\r\n脑宁胶囊的制备工艺及质量标准研究<br />\r\n生物工程系<br />\r\n王娟<br />\r\n重点项目<br />\r\n4G智能手机微焊点用新型钎料的研制<br />\r\n机电工程系<br />\r\n张春红<br />\r\n重点项目', 1458289387, 1458289387, 1458289387, 5, 0, 0, NULL, 0, 0, 1458289387, 0);
INSERT INTO stb_topics (`topic_id`, `node_id`, `uid`, `ruid`, `title`, `keywords`, `content`, `addtime`, `updatetime`, `lastreply`, `views`, `comments`, `favorites`, `closecomment`, `is_top`, `is_hidden`, `ord`, `hasImg`) VALUES (13, 18, 1, NULL, '校友通讯校友通讯校友通讯校友通讯', '', '校友通讯校友通讯校友通讯校友通讯校友通讯校友通讯校友通讯<br />\r\n<br />\r\n校友通讯校友通讯<br />\r\n校友通讯', 1458290112, 1458290112, 1458290112, 2, 0, 0, NULL, 0, 0, 1458290112, 0);
INSERT INTO stb_topics (`topic_id`, `node_id`, `uid`, `ruid`, `title`, `keywords`, `content`, `addtime`, `updatetime`, `lastreply`, `views`, `comments`, `favorites`, `closecomment`, `is_top`, `is_hidden`, `ord`, `hasImg`) VALUES (14, 16, 1, NULL, '校友风采', '', '校友风采<br />\r\n校友风采<br />\r\n校友风采<br />\r\n校友风采<br />\r\n校友风采<br />\r\n校友风采<br />\r\n校友风采<br />\r\n校友风采', 1458290144, 1458290144, 1458290144, 5, 0, 0, NULL, 0, 0, 1458290144, 0);
INSERT INTO stb_topics (`topic_id`, `node_id`, `uid`, `ruid`, `title`, `keywords`, `content`, `addtime`, `updatetime`, `lastreply`, `views`, `comments`, `favorites`, `closecomment`, `is_top`, `is_hidden`, `ord`, `hasImg`) VALUES (15, 21, 1, NULL, '软件08', '软件', '<img src=\"http://localhost/stbbs/uploads/image/201606/20160623162113_45361.jpg\" alt=\"\">', 1466670076, 1466738103, 1466670076, 6, 0, 0, NULL, 0, 0, 1466670076, 0);
INSERT INTO stb_topics (`topic_id`, `node_id`, `uid`, `ruid`, `title`, `keywords`, `content`, `addtime`, `updatetime`, `lastreply`, `views`, `comments`, `favorites`, `closecomment`, `is_top`, `is_hidden`, `ord`, `hasImg`) VALUES (16, 11, 1, NULL, 'test', '', '<img src=\"http://localhost/stbbs/uploads/image/201606/20160624103543_14508.jpg\" alt=\"\">', 1466735748, 1466735748, 1466735748, 1, 0, 0, NULL, 0, 0, 1466735748, 0);


