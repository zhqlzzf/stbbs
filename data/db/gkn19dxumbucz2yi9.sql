#
# TABLE STRUCTURE FOR: stb_classes
#

DROP TABLE IF EXISTS stb_classes;

CREATE TABLE `stb_classes` (
  `c_id` varchar(10) NOT NULL,
  `c_name` varchar(30) NOT NULL,
  `dep_id` varchar(20) NOT NULL,
  `inschoolyear` int(4) NOT NULL,
  `topic_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`c_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195701', '徐州农校57级', 'swgc', 1957, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200701', '园林高职07', 'swgc', 2007, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200801', '牧医高职08-1', 'swgc', 2008, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200802', '园艺高职08', 'swgc', 2008, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200803', '园高08-2', 'swgc', 2008, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200804', '生物技术高职08', 'swgc', 2008, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200805', '生物制药高职08-2', 'swgc', 2008, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200806', '牧医高职08-1', 'swgc', 2008, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200807', '牧医高职08-5', 'swgc', 2008, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200808', '牧医高职08-4', 'swgc', 2008, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200809', '牧医高职08-2', 'swgc', 2008, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200810', '牧医高职08-3', 'swgc', 2008, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200811', '园林高职08-1', 'swgc', 2008, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200812', '园林高职08-2', 'swgc', 2008, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200813', '园林高职08-3', 'swgc', 2008, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200814', '园艺高职08', 'swgc', 2008, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200815', '植保高职08', 'swgc', 2008, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200901', '机电高职09', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200902', '牧医高职09-4', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200903', '生技高09-2', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200904', '生物制药高职09', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200905', '药物制剂高职09', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200906', '园林高职09-1', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200907', '园林高职09-3', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200908', '园艺高职09', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200909', '植保高职09', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200910', '牧医高职09-3', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200911', '植保高职09', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200912', '园林高职09-1', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200913', '会计高职09', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200914', '机电高职09', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200915', '数控高职09-1', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200916', '数控高职09-2', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200917', '计算机高职09', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200918', '软件09', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200919', '生物技术高职09-1', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200920', '牧医高职09-4', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200921', '生物技术高职09-2', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200922', '生物制药高职09-1', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200923', '生物制药高职09-2', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200924', '牧医高职09-1', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200925', '牧医高职09-2', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200926', '园艺高职09', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200927', '园林高职09-2', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200928', '园林高职09-3', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200929', '计算机(图)高职09', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200930', '药物制剂高职09', 'swgc', 2009, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('201001', '宠物高职10', 'swgc', 2010, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('201002', '会计高职10', 'swgc', 2010, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('201003', '机电高职10', 'swgc', 2010, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('201004', '计算机高职10', 'swgc', 2010, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('201005', '牧医高职10-1', 'swgc', 2010, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('201006', '牧医高职10-2', 'swgc', 2010, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('201007', '牧医高职10-3', 'swgc', 2010, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('201008', '生物技术高职10-1', 'swgc', 2010, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('201009', '生物技术高职10-2', 'swgc', 2010, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('201010', '生物制药高职10', 'swgc', 2010, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('201011', '数控高职10', 'swgc', 2010, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('201012', '药物制剂高职10', 'swgc', 2010, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('201013', '园林高职10-1', 'swgc', 2010, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('201014', '园林高职10-2', 'swgc', 2010, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('201015', '园林高职10-3', 'swgc', 2010, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('201016', '园艺高职10', 'swgc', 2010, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('201017', '植保高职10', 'swgc', 2010, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('201018', '计高（图形图像）10', 'swgc', 2010, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('201101', '汽车运用与维修11', 'swgc', 2011, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('2012001', '计算机中专12', 'swgc', 2012, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('2012002', '会电中专12', 'swgc', 2012, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('2012003', '汽车运用与维修12', 'swgc', 2012, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197602', '农学农大76-2', 'swgc', 1976, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197601', '农学农大76-1', 'swgc', 1976, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197505', '农机农大75', 'swgc', 1975, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197504', '畜牧兽医农大75', 'swgc', 1975, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197503', '农学农大75-3', 'swgc', 1975, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197502', '农学农大75-2', 'swgc', 1975, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197501', '农学农大75-1', 'swgc', 1975, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196605', '畜牧兽医66-2', 'swgc', 1966, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196604', '畜牧兽医66-1', 'swgc', 1966, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196603', '农学66-3', 'swgc', 1966, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196602', '农学66-2', 'swgc', 1966, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196601', '农学66-1', 'swgc', 1966, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196505', '畜牧兽医65-2', 'swgc', 1965, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196504', '畜牧兽医65-1', 'swgc', 1965, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196503', '农学65-3', 'swgc', 1965, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196502', '农学65-2', 'swgc', 1965, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196501', '农学65-1', 'swgc', 1965, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196415', '畜牧兽医64-2', 'swgc', 1964, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196414', '畜牧兽医64-1', 'swgc', 1964, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196413', '农学64-3', 'swgc', 1964, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196412', '农学64-2', 'swgc', 1964, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196411', '农学64-1', 'swgc', 1964, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196010', '农学60-3', 'swgc', 1960, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196009', '农学60-2', 'swgc', 1960, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196008', '农学60-1', 'swgc', 1960, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196007', '农业机械化大专60', 'swgc', 1960, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196006', '植保60-2', 'swgc', 1960, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196005', '畜牧兽医大专60', 'swgc', 1960, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196004', '植保60-1', 'swgc', 1960, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196003', '农学大专60', 'swgc', 1960, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196002', '农学60干部中专', 'swgc', 1960, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('196001', '植保大专60', 'swgc', 1960, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195906', '农学59', 'swgc', 1959, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195905', '植保59-3', 'swgc', 1959, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195904', '植保59-2', 'swgc', 1959, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195903', '农学大专59', 'swgc', 1959, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195902', '植保59-1', 'swgc', 1959, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195901', '植保大专59', 'swgc', 1959, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195807', '畜牧兽医大专58', 'swgc', 1958, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195806', '农学58-2', 'swgc', 1958, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195805', '农学大专58', 'swgc', 1958, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195804', '农学58-1', 'swgc', 1958, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195803', '大学预科58', 'swgc', 1958, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195802', '农学58干部中专', 'swgc', 1958, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195801', '植保大专58', 'swgc', 1958, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195607', '农学56-3', 'swgc', 1956, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195606', '农学56-2', 'swgc', 1956, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195605', '农学56-1', 'swgc', 1956, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195604', '植保56-4', 'swgc', 1956, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195603', '植保56-3', 'swgc', 1956, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195602', '植保56-2', 'swgc', 1956, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195601', '植保56-1', 'swgc', 1956, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195502', '植保55-2', 'swgc', 1955, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195501', '植保55-1', 'swgc', 1955, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195408', '党政干部学员', 'swgc', 1954, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195402', '植保54-2', 'swgc', 1954, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('195401', '植保54-1', 'swgc', 1954, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197603', '农学农大76-3', 'swgc', 1976, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197604', '农学农大76-4', 'swgc', 1976, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197605', '畜牧兽医农大76', 'swgc', 1976, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197606', '果林农大76', 'swgc', 1976, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197607', '农机农大76', 'swgc', 1976, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197608', '水利农大76', 'swgc', 1976, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197801', '植保78', 'swgc', 1978, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197802', '农学78', 'swgc', 1978, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197803', '畜牧兽医78', 'swgc', 1978, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197804', '植保79', 'swgc', 1979, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197901', '农学79-1', 'swgc', 1979, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197902', '农学79-2', 'swgc', 1979, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('197903', '畜牧兽医79', 'swgc', 1979, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198005', '畜牧兽医80', 'swgc', 1980, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198001', '植保80', 'swgc', 1980, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198002', '植保81', 'swgc', 1980, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198003', '农学80-1', 'swgc', 1980, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198004', '农学80-2', 'swgc', 1980, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198101', '农学81', 'swgc', 1981, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198102', '畜牧兽医81', 'swgc', 1981, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198201', '植保82', 'swgc', 1982, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198202', '植保82-1', 'swgc', 1982, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198203', '农学82-1', 'swgc', 1982, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198204', '农学82-2', 'swgc', 1982, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198301', '植保83', 'swgc', 1983, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198302', '农学83-1', 'swgc', 1983, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198303', '农学83-2', 'swgc', 1983, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198304', '农学师资专科83-1', 'swgc', 1983, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198305', '农学师资专科83-2', 'swgc', 1983, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198401', '植保84-1', 'swgc', 1984, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198402', '植保84-2', 'swgc', 1984, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198403', '农学84', 'swgc', 1984, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198404', '农学师资专科84-1', 'swgc', 1984, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198405', '农学师资专科84-2', 'swgc', 1984, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198501', '植保85', 'swgc', 1985, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198502', '农学85', 'swgc', 1985, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198503', '商品检验85', 'swgc', 1985, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198504', '园艺85', 'swgc', 1985, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198505', '生物师资专科85', 'swgc', 1985, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198506', '农学师资专科85', 'swgc', 1985, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198507', '畜牧兽医师资专科85', 'swgc', 1985, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198601', '植保86', 'swgc', 1986, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198602', '农学86', 'swgc', 1986, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198603', '园艺86', 'swgc', 1986, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198604', '生物师资专科86', 'swgc', 1986, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198605', '畜牧兽医师资专科86', 'swgc', 1986, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198701', '农业推广87', 'swgc', 1987, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198702', '畜牧兽医推广87', 'swgc', 1987, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198703', '植保87', 'swgc', 1987, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198704', '园艺87', 'swgc', 1987, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198705', '畜牧兽医师资专科87', 'swgc', 1987, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198801', '农业推广88', 'swgc', 1988, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198802', '植保88', 'swgc', 1988, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198803', '园艺88', 'swgc', 1988, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198804', '畜牧兽医师资专科88', 'swgc', 1988, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198901', '农业推广89-1', 'swgc', 1989, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198902', '农业推广89-2', 'swgc', 1989, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198903', '畜牧兽医推广89', 'swgc', 1989, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198904', '园艺（春）89', 'swgc', 1989, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198905', '植保89', 'swgc', 1989, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('198906', '园艺89', 'swgc', 1989, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199001', '农业推广90-1', 'swgc', 1990, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199002', '农业推广90-2', 'swgc', 1990, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199003', '畜牧兽医推广90', 'swgc', 1990, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199004', '植保90', 'swgc', 1990, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199005', '园艺90', 'swgc', 1990, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199101', '农艺（春）91', 'swgc', 1991, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199102', '农业推广91-1', 'swgc', 1991, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199103', '农业推广91-2', 'swgc', 1991, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199104', '畜牧兽医推广91', 'swgc', 1991, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199105', '果树推广91', 'swgc', 1991, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199106', '植保91', 'swgc', 1991, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199107', '农学91', 'swgc', 1991, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199108', '园艺91', 'swgc', 1991, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199109', '农艺（春）91汉王', 'swgc', 1991, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199201', '农艺（春）92', 'swgc', 1992, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199202', '畜牧兽医（春）92', 'swgc', 1992, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199203', '植保92', 'swgc', 1992, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199204', '农学92', 'swgc', 1992, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199205', '畜牧兽医92', 'swgc', 1992, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199206', '果树92', 'swgc', 1992, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199207', '蔬菜92', 'swgc', 1992, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199301', '植保93', 'swgc', 1993, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199302', '果树93-1', 'swgc', 1993, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199303', '果树93-2', 'swgc', 1993, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199304', '蔬菜93', 'swgc', 1993, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199305', '果蔬加工93-1', 'swgc', 1993, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199306', '果蔬加工93-2', 'swgc', 1993, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199307', '经营管理93-1', 'swgc', 1993, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199308', '经营管理93-2', 'swgc', 1993, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199309', '市场营销93-1', 'swgc', 1993, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199310', '市场营销93-2', 'swgc', 1993, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199401', '畜牧兽医94', 'swgc', 1994, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199402', '果树94', 'swgc', 1994, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199403', '蔬菜94', 'swgc', 1994, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199404', '加工94', 'swgc', 1994, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199405', '经营管理94-1', 'swgc', 1994, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199406', '经营管理94-2', 'swgc', 1994, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199407', '营销94', 'swgc', 1994, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199408', '财会94-1', 'swgc', 1994, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199409', '财会94-2', 'swgc', 1994, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199410', '文秘94', 'swgc', 1994, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199501', '畜牧兽医推广95', 'swgc', 1995, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199502', '园推95', 'swgc', 1995, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199503', '植保95', 'swgc', 1995, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199504', '畜牧兽医95', 'swgc', 1995, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199505', '果树95', 'swgc', 1995, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199506', '蔬菜95', 'swgc', 1995, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199507', '加工95-1', 'swgc', 1995, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199508', '加工95-2', 'swgc', 1995, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199509', '经营管理95', 'swgc', 1995, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199510', '营销95', 'swgc', 1995, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199511', '农机95', 'swgc', 1995, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199512', '财会95-1', 'swgc', 1995, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199513', '财会95-2', 'swgc', 1995, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199514', '文秘95', 'swgc', 1995, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199601', '畜牧兽医推广96', 'swgc', 1996, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199602', '园推96', 'swgc', 1996, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199603', '植保96', 'swgc', 1996, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199604', '特养96', 'swgc', 1996, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199605', '果树96', 'swgc', 1996, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199606', '蔬菜96', 'swgc', 1996, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199607', '加工96', 'swgc', 1996, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199608', '经营管理96-1', 'swgc', 1996, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199609', '经营管理96-2', 'swgc', 1996, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199610', '营销96', 'swgc', 1996, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199611', '机电技术应用96-1', 'swgc', 1996, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199612', '机电技术应用96-2', 'swgc', 1996, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199613', '财会96-1', 'swgc', 1996, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199614', '财会96-2', 'swgc', 1996, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199615', '文秘96', 'swgc', 1996, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199701', '农业经济推广97', 'swgc', 1997, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199702', '企业管理推广97', 'swgc', 1997, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199703', '机电技术应用推广97', 'swgc', 1997, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199704', '植保97', 'swgc', 1997, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199705', '畜牧兽医97', 'swgc', 1997, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199706', '林果97', 'swgc', 1997, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199707', '蔬菜97', 'swgc', 1997, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199708', '加工97', 'swgc', 1997, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199709', '经营管理97', 'swgc', 1997, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199710', '营贸97-1', 'swgc', 1997, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199711', '营贸97-2', 'swgc', 1997, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199712', '机电技术应用97-1', 'swgc', 1997, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199713', '机电技术应用97-2', 'swgc', 1997, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199714', '财会97', 'swgc', 1997, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199715', '文秘97', 'swgc', 1997, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199801', '农业推广98', 'swgc', 1998, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199802', '植保98', 'swgc', 1998, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199803', '农学98-1', 'swgc', 1998, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199804', '农学98-2', 'swgc', 1998, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199805', '畜牧兽医98', 'swgc', 1998, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199806', '园艺98', 'swgc', 1998, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199807', '蔬菜98', 'swgc', 1998, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199808', '加工98', 'swgc', 1998, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199809', '经营管理98', 'swgc', 1998, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199810', '营销98', 'swgc', 1998, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199811', '机电技术应用98-1', 'swgc', 1998, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199812', '机电技术应用98-2', 'swgc', 1998, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199813', '财会98', 'swgc', 1998, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199814', '文秘98', 'swgc', 1998, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199901', '蔬菜推99', 'swgc', 1999, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199902', '经营管理99', 'swgc', 1999, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199903', '机电技术应用99', 'swgc', 1999, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199904', '财审99', 'swgc', 1999, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199905', '文秘99', 'swgc', 1999, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199906', '植保99-1', 'swgc', 1999, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199907', '植保99-2', 'swgc', 1999, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199908', '农艺99-1', 'swgc', 1999, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199909', '农艺99-2', 'swgc', 1999, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199910', '畜牧兽医99-1', 'swgc', 1999, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199911', '畜牧兽医99-2', 'swgc', 1999, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199912', '城镇规划与园林绿化99', 'swgc', 1999, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199913', '园艺99', 'swgc', 1999, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199914', '林果99', 'swgc', 1999, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199915', '蔬菜99-1', 'swgc', 1999, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('199916', '蔬菜99-2', 'swgc', 1999, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200001', '财经00', 'swgc', 2000, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200002', '计算机应用与维护00-1', 'swgc', 2000, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200003', '计算机应用与维护00-2', 'swgc', 2000, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200004', '计算机应用与维护00-3', 'swgc', 2000, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200005', '植保00', 'swgc', 2000, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200006', '农艺00', 'swgc', 2000, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200007', '畜牧兽医00-1', 'swgc', 2000, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200008', '畜牧兽医00-2', 'swgc', 2000, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200009', '畜牧兽医00-3', 'swgc', 2000, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200010', '城镇规划与园林绿化00', 'swgc', 2000, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200011', '园林00', 'swgc', 2000, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200012', '园艺00', 'swgc', 2000, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200013', '农艺高职00', 'swgc', 2000, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200014', '畜牧兽医高职00', 'swgc', 2000, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200015', '园高00', 'swgc', 2000, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200016', '蔬菜高00', 'swgc', 2000, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200101', '植保01', 'swgc', 2001, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200102', '农艺01', 'swgc', 2001, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200103', '畜牧兽医01-1', 'swgc', 2001, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200104', '畜牧兽医01-2', 'swgc', 2001, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200105', '畜牧兽医01-3', 'swgc', 2001, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200106', '园艺01', 'swgc', 2001, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200107', '机电技术应用01', 'swgc', 2001, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200108', '财会01', 'swgc', 2001, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200109', '计算机应用与维护01-1', 'swgc', 2001, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200110', '计算机应用与维护01-2', 'swgc', 2001, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200111', '计算机应用与维护01-3', 'swgc', 2001, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200112', '计算机应用与维护01-4', 'swgc', 2001, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200113', '畜牧兽医高职01', 'swgc', 2001, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200114', '园艺高职01', 'swgc', 2001, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200201', '畜牧兽医02-1', 'swgc', 2002, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200202', '畜牧兽医02-2', 'swgc', 2002, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200203', '畜牧兽医02-3', 'swgc', 2002, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200204', '园林02', 'swgc', 2002, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200205', '商贸02', 'swgc', 2002, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200206', '机电技术应用02', 'swgc', 2002, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200207', '计算机应用与维护02-1', 'swgc', 2002, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200208', '计算机应用与维护02-2', 'swgc', 2002, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200209', '计算机应用与维护02-3', 'swgc', 2002, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200210', '生物02', 'swgc', 2002, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200301', '环境与植物保护03', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200302', '畜牧兽医03-1', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200303', '畜牧兽医03-2', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200304', '畜牧兽医03-3', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200305', '畜牧兽医联办03', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200306', '园林绿化03', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200307', '机电技术应用03-1', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200308', '机电技术应用03-2', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200309', '机电技术应用03-3', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200310', '会计电算化03', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200311', '计算机应用与维护03-1', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200312', '计算机应用与维护03-2', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200313', '计算机应用与维护03-3', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200314', '计算机应用与维护03-4', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200315', '计算机应用与维护联办03', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200316', '网络03-1', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200317', '网络03-2', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200318', '商务英语03', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200319', '生物技术应用03', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200320', '生物技术应用联办03-1', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200321', '生物技术应用联办03-2', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200322', '生物制药03-1', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200323', '生物制药03-2', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200324', '生物制药03-3', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200325', '数控技术应用03', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200326', '车辆驾驶与维修03', 'swgc', 2003, 1);
INSERT INTO stb_classes (`c_id`, `c_name`, `dep_id`, `inschoolyear`, `topic_id`) VALUES ('200327', '法律03', 'swgc', 2003, 1);


#
# TABLE STRUCTURE FOR: stb_comments
#

DROP TABLE IF EXISTS stb_comments;

CREATE TABLE `stb_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL DEFAULT '0',
  `uid` int(11) NOT NULL DEFAULT '0',
  `content` text,
  `replytime` char(10) DEFAULT NULL,
  PRIMARY KEY (`id`,`topic_id`,`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

INSERT INTO stb_comments (`id`, `topic_id`, `uid`, `content`, `replytime`) VALUES (12, 16, 1, '很好很好', '1466746584');


#
# TABLE STRUCTURE FOR: stb_departments
#

DROP TABLE IF EXISTS stb_departments;

CREATE TABLE `stb_departments` (
  `dep_id` varchar(20) NOT NULL,
  `dep_name` varchar(20) NOT NULL,
  PRIMARY KEY (`dep_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO stb_departments (`dep_id`, `dep_name`) VALUES ('swgc', '整个学院');


#
# TABLE STRUCTURE FOR: stb_favorites
#

DROP TABLE IF EXISTS stb_favorites;

CREATE TABLE `stb_favorites` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `favorites` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `content` mediumtext NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: stb_inschoolyear
#

DROP TABLE IF EXISTS stb_inschoolyear;

CREATE TABLE `stb_inschoolyear` (
  `yid` int(4) NOT NULL,
  PRIMARY KEY (`yid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO stb_inschoolyear (`yid`) VALUES (1953);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1954);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1955);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1956);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1957);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1958);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1959);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1960);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1961);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1962);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1963);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1964);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1965);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1966);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1967);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1968);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1969);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1970);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1971);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1972);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1973);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1974);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1975);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1976);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1977);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1978);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1979);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1980);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1981);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1982);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1983);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1984);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1985);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1986);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1987);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1988);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1989);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1990);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1991);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1992);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1993);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1994);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1995);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1996);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1997);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1998);
INSERT INTO stb_inschoolyear (`yid`) VALUES (1999);
INSERT INTO stb_inschoolyear (`yid`) VALUES (2001);
INSERT INTO stb_inschoolyear (`yid`) VALUES (2002);
INSERT INTO stb_inschoolyear (`yid`) VALUES (2003);
INSERT INTO stb_inschoolyear (`yid`) VALUES (2004);
INSERT INTO stb_inschoolyear (`yid`) VALUES (2005);
INSERT INTO stb_inschoolyear (`yid`) VALUES (2006);
INSERT INTO stb_inschoolyear (`yid`) VALUES (2007);
INSERT INTO stb_inschoolyear (`yid`) VALUES (2008);
INSERT INTO stb_inschoolyear (`yid`) VALUES (2009);
INSERT INTO stb_inschoolyear (`yid`) VALUES (2010);
INSERT INTO stb_inschoolyear (`yid`) VALUES (2011);
INSERT INTO stb_inschoolyear (`yid`) VALUES (2012);
INSERT INTO stb_inschoolyear (`yid`) VALUES (2013);
INSERT INTO stb_inschoolyear (`yid`) VALUES (2014);
INSERT INTO stb_inschoolyear (`yid`) VALUES (2015);
INSERT INTO stb_inschoolyear (`yid`) VALUES (2016);


#
# TABLE STRUCTURE FOR: stb_links
#

DROP TABLE IF EXISTS stb_links;

CREATE TABLE `stb_links` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `logo` varchar(200) DEFAULT NULL,
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO stb_links (`id`, `name`, `url`, `logo`, `is_hidden`) VALUES (1, 'StartBBS', 'http://www.startbbs.com', '', 1);
INSERT INTO stb_links (`id`, `name`, `url`, `logo`, `is_hidden`) VALUES (2, '徐州生物工程职业技术学院', 'http://www.xzsw.net', NULL, 0);
INSERT INTO stb_links (`id`, `name`, `url`, `logo`, `is_hidden`) VALUES (3, '矿大', 'http://www.cumt.edu.cn', NULL, 0);


#
# TABLE STRUCTURE FOR: stb_message
#

DROP TABLE IF EXISTS stb_message;

CREATE TABLE `stb_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dialog_id` int(11) NOT NULL,
  `sender_uid` int(11) NOT NULL,
  `receiver_uid` int(11) NOT NULL,
  `content` text NOT NULL,
  `create_time` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dialog_id` (`dialog_id`),
  KEY `sender_uid` (`sender_uid`),
  KEY `create_time` (`create_time`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO stb_message (`id`, `dialog_id`, `sender_uid`, `receiver_uid`, `content`, `create_time`) VALUES (1, 1, 1, 12, 'asd', 1474519380);


#
# TABLE STRUCTURE FOR: stb_message_dialog
#

DROP TABLE IF EXISTS stb_message_dialog;

CREATE TABLE `stb_message_dialog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_uid` int(11) NOT NULL,
  `receiver_uid` int(11) NOT NULL,
  `last_content` text NOT NULL,
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  `sender_remove` tinyint(1) NOT NULL DEFAULT '0',
  `receiver_remove` tinyint(1) NOT NULL DEFAULT '0',
  `sender_read` tinyint(1) NOT NULL DEFAULT '1',
  `receiver_read` tinyint(1) NOT NULL DEFAULT '0',
  `messages` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `uid` (`sender_uid`,`receiver_uid`),
  KEY `update_time` (`update_time`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO stb_message_dialog (`id`, `sender_uid`, `receiver_uid`, `last_content`, `create_time`, `update_time`, `sender_remove`, `receiver_remove`, `sender_read`, `receiver_read`, `messages`) VALUES (1, 1, 12, 'asd', 1474519380, 1474519380, 0, 0, 1, 0, 1);


#
# TABLE STRUCTURE FOR: stb_nodes
#

DROP TABLE IF EXISTS stb_nodes;

CREATE TABLE `stb_nodes` (
  `node_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `pid` smallint(5) NOT NULL DEFAULT '0',
  `sortid` int(11) NOT NULL DEFAULT '0',
  `cname` varchar(30) DEFAULT NULL COMMENT '分类名称',
  `content` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `ico` varchar(128) NOT NULL DEFAULT 'uploads/ico/default.png',
  `master` varchar(100) NOT NULL,
  `permit` varchar(255) DEFAULT NULL,
  `listnum` int(8) DEFAULT '0',
  `clevel` varchar(25) DEFAULT NULL,
  `cord` smallint(6) DEFAULT NULL,
  `node_type` varchar(10) NOT NULL DEFAULT 'normal',
  `listDisplay_style` varchar(10) DEFAULT 'news',
  PRIMARY KEY (`node_id`,`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`, `listDisplay_style`) VALUES (28, 0, 0, '祝福母校', '', '', 'uploads/ico/default.png', '', '1,2,6,4', 2, NULL, NULL, 'normal', 'news');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`, `listDisplay_style`) VALUES (3, 0, 0, '校庆专栏', '', '', 'uploads/ico/default.png', '', NULL, 0, NULL, NULL, 'normal', 'news');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`, `listDisplay_style`) VALUES (4, 3, 0, '筹备', '', '', 'uploads/ico/default.png', '', NULL, 2, NULL, NULL, 'normal', 'news');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`, `listDisplay_style`) VALUES (11, 0, 1, '我的母校', '', '', 'uploads/ico/default.png', '', NULL, 2, NULL, NULL, 'normal', 'news');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`, `listDisplay_style`) VALUES (12, 11, 0, '相册', '', '', 'uploads/ico/default.png', '', NULL, 6, NULL, NULL, 'normal', 'news');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`, `listDisplay_style`) VALUES (13, 11, 0, '校园', '', '', 'uploads/ico/default.png', '', NULL, 1, NULL, NULL, 'normal', 'news');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`, `listDisplay_style`) VALUES (14, 0, 0, '校友工作动态', '', '', 'uploads/ico/default.png', '', NULL, 4, NULL, NULL, 'normal', 'news');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`, `listDisplay_style`) VALUES (15, 0, 1, '通知公告', '', '', 'uploads/ico/default.png', '', NULL, 3, NULL, NULL, 'normal', 'news');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`, `listDisplay_style`) VALUES (16, 0, 0, '校友风采', '', '', 'uploads/ico/default.png', '', NULL, 11, NULL, NULL, 'normal', 'news');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`, `listDisplay_style`) VALUES (17, 0, 1, '校友捐赠', '校友不断支持学院发展', '', 'uploads/ico/default.png', '', NULL, 0, NULL, NULL, 'normal', 'news');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`, `listDisplay_style`) VALUES (18, 0, 0, '校友通讯', '', '', 'uploads/ico/default.png', '', NULL, 0, NULL, NULL, 'normal', 'news');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`, `listDisplay_style`) VALUES (19, 0, 0, '固定栏目', '', '', 'uploads/ico/default.png', '', '1,2', 0, NULL, NULL, 'normal', 'news');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`, `listDisplay_style`) VALUES (20, 19, 0, '首页大图', '', '', 'uploads/ico/default.png', '', '1,6', 4, NULL, NULL, 'bigPic', 'news');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`, `listDisplay_style`) VALUES (21, 19, 0, '班级相册', '欢迎来到班级列表，你可以在下方的搜索框里搜索相应的班级', '', 'uploads/ico/default.png', '', '1,2,4', -1, NULL, NULL, 'bigPic', 'news');
INSERT INTO stb_nodes (`node_id`, `pid`, `sortid`, `cname`, `content`, `keywords`, `ico`, `master`, `permit`, `listnum`, `clevel`, `cord`, `node_type`, `listDisplay_style`) VALUES (22, 19, 0, '班级列表', '欢迎来到班级列表，你可以在下方的搜索框里搜索相应的班级', '', 'uploads/ico/default.png', '', '1,2,6,4', -3, NULL, NULL, 'news', 'news');


#
# TABLE STRUCTURE FOR: stb_notifications
#

DROP TABLE IF EXISTS stb_notifications;

CREATE TABLE `stb_notifications` (
  `nid` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) DEFAULT NULL,
  `suid` int(11) DEFAULT NULL,
  `nuid` int(11) NOT NULL DEFAULT '0',
  `ntype` tinyint(1) DEFAULT NULL,
  `ntime` int(10) DEFAULT NULL,
  PRIMARY KEY (`nid`,`nuid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: stb_page
#

DROP TABLE IF EXISTS stb_page;

CREATE TABLE `stb_page` (
  `pid` tinyint(6) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  `go_url` varchar(100) DEFAULT NULL,
  `add_time` int(10) DEFAULT NULL,
  `is_hidden` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO stb_page (`pid`, `title`, `content`, `go_url`, `add_time`, `is_hidden`) VALUES (1, '单页面测试', '									单页面测试单页面测试单页面测试单页面测试单页面测试								', '', 1473152778, 0);
INSERT INTO stb_page (`pid`, `title`, `content`, `go_url`, `add_time`, `is_hidden`) VALUES (2, '跳转', '																	', 'http://2016.swu.edu.cn/site/xq/xn/index/', 1474157558, 1);


#
# TABLE STRUCTURE FOR: stb_settings
#

DROP TABLE IF EXISTS stb_settings;

CREATE TABLE `stb_settings` (
  `id` tinyint(5) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `type` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`title`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (1, 'site_name', '徐州生物工程职业技术学院-校友系统', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (2, 'welcome_tip', '欢迎回到母校', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (3, 'short_intro', '2016年是我校建校60周年', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (4, 'show_captcha', 'on', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (5, 'site_run', '0', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (6, 'site_stats', '统计代码																																																																																																																																																										', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (7, 'site_keywords', '徐州生物工程职业技术学院,校友系统,校庆', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (8, 'site_description', '徐州生物工程职业技术学院,校友系统,校庆', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (9, 'money_title', '银币', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (10, 'per_page_num', '0', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (11, 'is_rewrite', 'off', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (12, 'show_editor', 'on', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (13, 'comment_order', '0', 0);
INSERT INTO stb_settings (`id`, `title`, `value`, `type`) VALUES (14, 'bigPicCounts', '5', 0);


#
# TABLE STRUCTURE FOR: stb_site_stats
#

DROP TABLE IF EXISTS stb_site_stats;

CREATE TABLE `stb_site_stats` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `item` varchar(20) NOT NULL,
  `value` int(10) DEFAULT '0',
  `update_time` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

INSERT INTO stb_site_stats (`id`, `item`, `value`, `update_time`) VALUES (1, 'last_uid', 19, NULL);
INSERT INTO stb_site_stats (`id`, `item`, `value`, `update_time`) VALUES (2, 'total_users', 17, NULL);
INSERT INTO stb_site_stats (`id`, `item`, `value`, `update_time`) VALUES (3, 'today_topics', -1, 1477616800);
INSERT INTO stb_site_stats (`id`, `item`, `value`, `update_time`) VALUES (4, 'yesterday_topics', 7, 1477616205);
INSERT INTO stb_site_stats (`id`, `item`, `value`, `update_time`) VALUES (5, 'total_topics', 44, NULL);
INSERT INTO stb_site_stats (`id`, `item`, `value`, `update_time`) VALUES (6, 'total_comments', 4, NULL);
INSERT INTO stb_site_stats (`id`, `item`, `value`, `update_time`) VALUES (7, 'total_nodes', 0, NULL);
INSERT INTO stb_site_stats (`id`, `item`, `value`, `update_time`) VALUES (8, 'total_tags', 0, NULL);


