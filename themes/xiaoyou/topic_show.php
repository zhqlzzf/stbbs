<!DOCTYPE html>
<html>
<head>
<title><?php echo $content['title']?> - <?php echo $settings['site_name']?></title>
<meta charset='UTF-8'>
<meta content='True' name='HandheldFriendly'>
<meta content='width=device-width, initial-scale=1.0' name='viewport'>
<meta name="keywords" content="<?php echo $content['keywords']?>" />
<meta name="description" content="<?php echo $content['description'];?>" />
<?php $this->load->view('common/xyheader-meta');?>
<script src="<?php echo base_url('static/xiaoyou/js/topic.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('static/common/js/plugins.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('static/common/js/jquery.upload.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('static/layer-v2.1/layer/layer.js')?>" type="text/javascript"></script>
<?php if($this->config->item('storage_set')=='local'){?>
<script src="<?php echo base_url('static/xiaoyou/js/local.file.js')?>" type="text/javascript"></script>
<?php } else{?>
<script src="<?php echo base_url('static/common/js/qiniu.js')?>" type="text/javascript"></script>
<?php }?>
</head>
<body >
<?php $this->load->view('common/xyheader'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading topic-detail-heading">
                        <p><a href="<?php echo base_url();?>">首页</a> / <a href="<?php echo site_url('node/show/'.$cate['node_id']);?>"><?php echo $cate['cname'];?></a></p>
                        <h2 class="panel-title"><?php echo $content['title']?></h2>
                        <div class="text-muted hints">
                            <span>By <a href="<?php echo site_url('user/profile/'.$content['uid']);?>"><?php echo $content['username']; ?></a></span>&nbsp;•&nbsp;
                            <span><?php echo date('Y-m-d H:i:s',$content['addtime']);?></span>&nbsp;•&nbsp;来自于<?php echo $logininfo['country'].$logininfo['province'].$logininfo['city'].$logininfo['isp'].$logininfo['ip'];?>&nbsp;•&nbsp;<span><?php echo $content['views']?>次点击</span>
							
                        </div>
                    </div>
                    <?php if($page==1){?>
                    <div class="panel-body content">
						<?php  ?>
                        <?php echo $content['content']?>
                        <?php if(isset($tag_list)){?>
						<p class="tag">
						<?php foreach($tag_list as $tag){?>
						<a href='<?php echo site_url($tag['tag_url']);?>'><?php echo $tag['tag_title'];?></a>&nbsp;
						<?php }?>
						</p>
						<?php }?>

                    </div>
                    <?php }?>
                    <div class="panel-footer">
						<?php if($this->auth->is_admin() || $this->auth->is_master($cate['node_id'])){?>
						
						<a href="<?php echo site_url('topic/edit/'.$content['topic_id']);?>" class="btn btn-default btn-sm unbookmark" data-method="edit" rel="nofollow">编辑</a>
						<a href="javascript:if(confirm('确实要删除吗?'))location='<?php echo site_url('topic/del/'.$content['topic_id'].'/'.$content['node_id'].'/'.$content['uid']);?>'" class="btn btn-sm btn-danger" data-method="edit" rel="nofollow">删除</a>
						<?php }?>
						<?php if($this->auth->is_admin() || $this->auth->is_master($cate['node_id'])){?>
						<a href="<?php echo site_url('topic/show/'.$content['topic_id'].'?act=set_top');?>" class="btn btn-default btn-sm unbookmark" data-method="edit" rel="nofollow">
						<?php if($content['is_top']==0){?>
						置顶
						<?php } else {?>
						取消置顶
						<?php }?>
						</a>
						<?php }?>
                    </div>
                </div><!-- /.panel content -->
                <div id="error"></div>
            </div><!-- /.col-md-8 -->

			<div class="col-md-4">
			<?php $this->load->view('common/sidebar_login');?>
			<?php $this->load->view('common/sidebar_cateinfo');?>
			<?php $this->load->view('common/sidebar_cates');?>
			<?php $this->load->view('common/sidebar_related_topic');?>
			<?php $this->load->view('common/sidebar_ad');?>
			</div><!-- /.col-md-4 -->

        </div><!-- /.row -->
    </div><!-- /.container -->
<div class="modal" id="mymodal">
    <div class="modal-dialog">
        <div class="modal-content">
		
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">请登录</h4>
			</div>
			<div class="modal-body">
				<div class="panel-body">
				
					<div class="form-group">
						<label class=" control-label" for="user_nickname">用户名</label>
						<div class="">
						<input class="form-control" id="user_nickname" name="username" size="50" type="text" value=""><span class="help-block red"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label" for="user_password">密码</label>
						<div class="">
						<input class="form-control" id="user_password" name="password" size="50" type="password" value="">
						<span class="help-block red"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label" for="captcha_code">验证码</label>
						<div class="">
						<input class="form-control" id="captcha_code" name="captcha_code" size="50" type="text" value="">
						<span class="help-block red"></span>
						</div>
						<div class="">
						<a href="javascript:reloadcode();" title="更换验证码"><img src="http://localhost/stbbs/index.php/captcha_code" name="checkCodeImg" id="checkCodeImg" border="0"></a>&nbsp;&nbsp;<a href="javascript:reloadcode();">换一张</a>
						</div>
					</div>
					<script language="javascript">
					//刷新图片
					function reloadcode() {//刷新验证码函数
					 var verify = document.getElementById('checkCodeImg');
					 verify.setAttribute('src', 'http://localhost/stbbs/index.php/captcha_code?' + Math.random());
					}
					</script>
					<div class="form-group">
						<div class="sub">
							<button class="btn btn-primary" id="btnLogin">登入</button>
							<a href="http://localhost/stbbs/index.php/user/findpwd" class="btn btn-default" role="button">找回密码</a>
						</div>
					</div>
				
                </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary">保存</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal-backdrop  in"></div>
<div id="cover" class="cover"></div>
<?php $this->load->view('common/footer');?>
</body>
</html>