<?php if($this->session->userdata('uid')){ ?>
<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
	            <ul class="list-unstyled">
	            	<li><a href="<?php echo site_url('user/profile/'.$myinfo['uid']);?>" title="<?php echo $myinfo['username']?>"><?php echo $myinfo['username']?></a>  • 用户组：<?php echo $myinfo['group_name']?></li>
	            </ul>
            </div>
        </div>
    </div>
    <div class="panel-footer text-muted">
		<?php if($myinfo['notices']){?>
		<img align="top" alt="Dot_orange" class="icon" src="<?php echo base_url('static/common/images/dot_orange.png');?>" />
		<a href="<?php echo site_url('notifications');?>"><?php echo $myinfo['notices']?> 条未读提醒</a>
		<?php } else{?>
		暂无提醒
		<?php }?>
	</div>
</div>
<?php } else {?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4><?php echo $settings['site_name']?></h4>
    </div>
    <div class="panel-body">
        <a href="<?php echo site_url('user/register');?>" class="btn btn-default">现在注册</a>
		或者<a href="<?php echo site_url('claim');?>" class="btn btn-default">校友认领</a><br>	已经注册请<a href="<?php echo site_url('user/login');?>" class="startbbs">登入</a>
		<hr>亲爱的历届校友：我们整理了自1954年至今的所有校友数据，推荐使用 校友认领 功能来认领自己的用户。
    </div>
</div>
<?php }?>