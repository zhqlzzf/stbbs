<!DOCTYPE html>
<html>
<head>
<meta charset='UTF-8'>
<meta content='width=device-width, initial-scale=1.0' name='viewport'>
<title><?php echo $title?>- <?php echo $settings['site_name']?></title>
<meta name="keywords" content="<?php echo $title?>" />
<meta name="description" content="<?php echo $category['content'];?>" />
<?php $this->load->view('common/xyheader-meta');?>
</head>
<body id="startbbs">
<?php $this->load->view('common/xyheader');?>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo $category['cname'];?> 栏目简介:<span class="pull-right">文章&nbsp;<span class='badge badge-info'><?php echo $category['listnum'];?></span></span></h3>
                    </div>
                    <div class="panel-body">
                        <p class="text-muted">
                        <?php echo $category['content'];?>
                        </p>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">文章列表<small>(管理员:<?php echo $category['master'];?>)</small></h3>
                    </div>
                    <div class="panel-body">
	                    <?php if($topic_list):?>
                        <ul class="media-list">
							<?php foreach($topic_list as $v):?>
                            <li class="media topic-list">
                                
                                <div class="media-body">
                                    <h4 class="media-heading"><a target="_blank" href="<?php echo url('topic_show',$v['topic_id']);?>"><?php echo $v['title'];?></a><?php if( $v['is_top'] == '1' ) echo '<span class="badge badge-info">置顶</span>'; ?>
									<span><?php echo date("Y/m/d",$v['updatetime'])?></span></h4>
                                </div>
                            </li>
						<?php endforeach;?>
                        </ul>
                        <nav>
                        <?php if($pagination):?><ul class="pager"><?php echo $pagination;?></ul><?php endif?></nav>
						<?php else:?>
						暂无话题
						<?php endif?>
                    </div>
                </div>
            </div><!-- /.col-md-8 -->

            <div class="col-md-4">
			<?php $this->load->view('common/sidebar_login');?>
			<?php $this->load->view('common/sidebar_cates');?>
			<!--<?php $this->load->view('common/sidebar_ad');?>-->
            </div><!-- /.col-md-4 -->

        </div><!-- /.row -->
    </div><!-- /.container -->

<?php $this->load->view('common/footer');?>
</body>
</html>