<!DOCTYPE html>
<html>
<head>
	<meta charset='UTF-8' />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo $settings['site_name']?>-<?php echo $settings['short_intro']?></title>
	<meta name="keywords" content="<?php echo $settings['site_keywords']?>" />
	<meta name="description" content="<?php echo $settings['short_intro']?>" />
	<?php $this->load->view('common/xyheader-meta');?>
	<!--大图轮播开始-->
	<script type="text/javascript" src="<?php echo base_url('static/lunbo/js/sl.js');?>"></script>
	<link href="<?php echo base_url('static/lunbo/lunbo.css');?>" media="screen" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		$(document).ready(function(){
			$.focus("#focus001");
		});
	</script>
	<!--大图轮播结束-->
	<!--图片新闻轮播开始-->
	<script type="text/javascript" src="<?php echo base_url('static/lunbo-1/js/main.js');?>"></script>
	<link href="<?php echo base_url('static/lunbo-1/css/style.css');?>" media="screen" rel="stylesheet" type="text/css" />
	<!--图片新闻轮播结束-->

</head>
<body style="zoom: 1;">
<div class="wp-inner">
    <?php $this->load->view('common/xyNav');?>
</div>
<!-- /*头部结束*/--> 
<div class="wp-inner">
    <div id="banner">
		<div class="focus" id="focus001">
			<ul >
			<?php foreach($bigpiclist as $pl) {?>
				<li style="background: url(<?php echo $pl?>) no-repeat;"></li>
			<?php }?>
			</ul>
        </div>
	</div>
</div>
<div class="wp-inner">
    <div class="conter banner clearfix"> 
    <!--左边大块-->
		<div class="center_left">
			<div class="conter_left_innder">
				<h2><a href="<?php echo site_url('pics_show');?>">图片新闻</a></h2>
				<div class="con_l_img">
					<div class="weichuangyi">
						<div class="weichuangyi_main">
							<ul class="weichuangyi_ul">
							<?php foreach($newspiclist as $npl) {?>
								<li><a href="<?php echo site_url('topic/show').'/'.$npl['tid'];?>" class="img" target="_blank"><img src="<?php echo $npl['url']?>"  alt="" /></a></li>
							<?php }?>
							</ul>
						</div>
						<div class="weichuangyi_span">
							<p> <span class="weichuangyi_span_one"></span> <span></span> <span></span> <span></span></p>
						</div>
					</div>
				</div>
			</div>
			<!--左边下部分-->
			<div class="conter_left_innder1">
				<h2><span><a href="<?php echo site_url('user');?>">最新加入校友</a></span></h2>
				<div class="center_r_top">
					
					<ul class="lastuser-list" id="topic_list">
					<?php if (!empty($lastreguserslist)) foreach($lastreguserslist as $v){ ?>
						<li class="media "> <a class="media-left" href="<?php echo site_url('user/profile').'/'.$v['uid'];?>" target="_blank"><img class="img-rounded medium" src="<?php echo site_url('../uploads/avatar/default/normal.png'); ?>" ></a>
							<div class="media-body">
								<p class="text-muted"> <span><a href="<?php echo site_url('user/profile').'/'.$v['uid'];?>" target="_blank"><?php echo $v['username'];?></a></span><br>
								<span><?php echo $v['inschoolyear'].' • '.$v['c_name'];?></span> </p>
							</div>
						</li>
					<?php }?>	
					</ul>
					
				</div>
			</div>
        </div>
		<!--中间大块-->
		<div class="center_center">
			<div class="center_center_inner">
				<?php $this->load->model('topic_m'); $data=$this->topic_m->get_topics_list2(14,5,18);
				//print_r($data14);
				//echo $this->db->last_query();	
				?>
				<h2><span><a href="<?php echo site_url('node/show').'/'.$data[0]['node_id'];?>" target="_blank"><?php echo $data[0]['cname'];?></a></span></h2>
				<div class="center_r_top">				
					<ul class="resource">
					<?php if (!empty($data)) foreach($data as $v){ ?>
						<li><a href="<?php echo site_url('topic/show').'/'.$v['topic_id'];?>" title="<?php echo $v['title'];?>" target="_blank"><?php echo $v['title'].'...';?></a><span><?php echo $v['updatetime'];?></span></li>
					<?php }?>	
					</ul>				
					<p class="news_more more" id="p-source"><a href="<?php echo site_url('node/show').'/'.$data[0]['node_id'];?>" target="_blank">更多&gt;&gt;</a></p>
				</div>
			</div>
			<!--中下部分-->
			<div class="center_center_inner1">
				<?php $this->load->model('topic_m'); $data=$this->topic_m->get_topics_list2(28,5,18);
				//print_r($data14);
				//echo $this->db->last_query();	
				?>
				<h2><span style="float:left;"><a href="<?php echo site_url('node/show').'/'.$data[0]['node_id'];?>" target="_blank"><?php echo $data[0]['cname'];?></a></span><span style="float:right;"><a href="<?php echo site_url('topic/add_zhufu/28');?>" target="_blank">>发表祝福< </a></span></h2>
				<div style="clear:both"></div>
				<div class="center_r_top ">
					<ul class="resource">
					<?php if (!empty($data)) foreach($data as $v){ ?>
						<li><a href="<?php echo site_url('topic/show').'/'.$v['topic_id'];?>" title="<?php echo $v['title'];?>" target="_blank"><?php $loginfo=$this->myclass->GetIpLookup($v['ip']); echo '【'.$loginfo['city'].'】'.$v['title'].'...';?></a><span><?php echo $v['updatetime'];?></span></li>
					<?php }?>
					</ul>
					<p class="news_more more " id="p-source"><a href="<?php echo site_url('node/show').'/'.$data[0]['node_id'];?>" target="_blank">更多&gt;&gt;</a></p>
				</div>
			</div>
        </div>
    
		<!--中间大块end--> 
		<!-- 右侧部分 -->
		<div class="center_right">
			<div class="center_right_inner1">
			<?php $this->load->model('topic_m'); $data=$this->topic_m->get_topics_list2(15,5,14);
			//print_r($data);
			//echo $this->db->last_query();	
			?>
				<h2><span><a href="<?php echo site_url('node/show').'/'.$data[0]['node_id'];?>" target="_blank"><?php echo $data[0]['cname'];?></a></span></h2>
				<div class="center_r_top">				
					<ul class="resource">
					<?php foreach($data as $v){ ?>
						<li><a href="<?php echo site_url('topic/show').'/'.$v['topic_id'];?>" title="<?php echo $v['title'];?>" target="_blank"><?php echo $v['title'].'...';?></a><span><?php echo $v['updatetime'];?></span></li>
					<?php }?>	
					</ul>				
					<p class="news_more more" id="p-source"><a href="<?php echo site_url('node/show').'/'.$data[0]['node_id'];?>">更多&gt;&gt;</a></p>
				</div>        
				<!-- 登录框-->        
				<p class="campus_but">登录校友网</p>
				<div class="center_register">
					<div class="cont_submit clearfix">
					<?php if($this->session->userdata('uid')==''){ ?>
						<a href="<?php echo site_url('user/login');?>" class="">登陆</a> <a href="<?php echo site_url('user/register');?>" class="">注册</a> <a href="<?php echo site_url('user/claim');?>" class="">认领</a>
					<?php } else if ($this->session->userdata('group_type')=='0') {?>
            <a href="<?php echo site_url('user/profile/'.$this->session->userdata('uid').'')?>" target="_blank" ><?php echo $this->session->userdata('username');?></a>，您可以<a href="<?php echo site_url('admin/login')?>" target="_blank" > 进入管理 </a>和<a href="<?php echo site_url('user/logout/'.$this->session->userdata('uid').'')?>" target="_self" > 注销登陆 </a>
            <?php } else {?>
            <a href="<?php echo site_url('user/profile/'.$this->session->userdata('uid').'')?>" target="_blank" ><?php echo $this->session->userdata('username');?></a>，您可以<a href="<?php echo site_url('classes/show/'.$c_id);?>" target="_blank" > 查看我的班级 </a><a href="<?php echo site_url('search/');?>" target="_blank" > 寻人 </a>和<a href="<?php echo site_url('user/logout/'.$this->session->userdata('uid').'')?>" target="_self" > 注销登陆 </a>
            <?php } ?>
					</div>
				</div>
				<!--23-->
				<div class="xytx">
				<?php $this->load->model('topic_m'); $data=$this->topic_m->get_topics_list2(16,5,20);
				//print_r($data);
				//echo $this->db->last_query();	
				?>
              <h2><span><a href="<?php echo site_url('node/show').'/'.$data[0]['node_id'];?>"><?php echo $data[0]['cname'];?></a></span></h2>
              <div class="center_r_top">
            
            <ul class="resource"><?php foreach($data as $v){ ?>
                  <li><a href="<?php echo site_url('topic/show').'/'.$v['topic_id'];?>" title="<?php echo $v['title'];?>" target="_blank"><?php echo $v['title'].'...';?></a><span><?php echo $v['updatetime'];?></span></li>
				  <?php }?>
                </ul>
            
            <p class="news_more more" id="p-source"><a href="<?php echo site_url('node/show').'/'.$data[0]['node_id'];?>">更多&gt;&gt;</a></p>
          </div>
            </div>
      </div>
        </div>
  </div>
    </div>

<!--右侧结束-->
<div class="wp-inner" style="display:none;">
      <div class="tradition">
    <div class="tradition_inner"> 
          <!--标题-->
          <div class="center_bottom_inner special">
        <h2><span><a href="javascript:void(0)" style="cursor:default">浮光掠影</a></span></h2>
      </div>
          <!--标题end--> 
          <!--内容-->
          <div class="indexmaindiv" id="indexmaindiv">
        <div class="indexmaindiv1 clearfix">
              <div class="stylesgoleft" id="goleft"></div>
              <div class="maindiv1 " id="maindiv1">
            <ul id="count1" style="width: 2123px; left: -772px;">
                  <li> </li>
                </ul>
          </div>
              <div class="stylesgoright" id="goright"></div>
            </div>
      </div>
          <!--友情链接-->
          <div class="center_rt_bottom clearfix"> <span class="friendship">友情链接</span>
        <?php  foreach($links as $r){?>
        <a href="<?php echo $r['url']; ?>" target="_blank" title="<?php echo $r['name']; ?>"><?php echo $r['name']; ?></a>
        <?php }?>
      </div>
          <!-- 友情链接end--> 
          <!--内容end--> 
        </div>
  </div>
    </div>
<!--/*校风结束*/-->
<?php $this->load->view('common/xyFooter');?>
</body>
</html>
