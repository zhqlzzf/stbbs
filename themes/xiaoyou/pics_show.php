<!DOCTYPE html>
<html>
<head>
<title><?php echo $content['title']?> - <?php echo $settings['site_name']?></title>
<meta charset='UTF-8'>
<meta content='True' name='HandheldFriendly'>
<meta content='width=device-width, initial-scale=1.0' name='viewport'>
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php $this->load->view('common/xyheader-meta');?>
<script src="<?php echo base_url('static/xiaoyou/js/topic.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('static/common/js/plugins.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('static/common/js/jquery.upload.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('static/layer-v2.1/layer/layer.js')?>" type="text/javascript"></script>
<?php if($this->config->item('storage_set')=='local'){?>
<script src="<?php echo base_url('static/xiaoyou/js/local.file.js')?>" type="text/javascript"></script>
<?php } else{?>
<script src="<?php echo base_url('static/common/js/qiniu.js')?>" type="text/javascript"></script>
<?php }?>
</head>
<body >
<?php $this->load->view('common/xyheader'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading topic-detail-heading">
                        <p><a href="<?php echo base_url();?>">首页</a> / 站内图片</p>
                    </div>
                    <?php if(1==1){?>
                    <div class="panel-body content">
						<ul class="pics_list">
							<?php foreach($newspiclist as $npl) {?>
								<li><a href="<?php echo site_url('topic/show').'/'.$npl['tid'];?>" class="img" target="_blank"><img src="<?php echo $npl['url']?>"  alt="" /></a></li>
							<?php }?>
							</ul>

                    </div>
                    <?php }?>
                    <div class="panel-footer">
                    </div>
                </div><!-- /.panel content -->
                <div id="error"></div>
            </div><!-- /.col-md-8 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
<div class="modal" id="mymodal">
    <div class="modal-dialog">
        <div class="modal-content">
		
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">请登录</h4>
			</div>
			<div class="modal-body">
				<div class="panel-body">
				
					<div class="form-group">
						<label class=" control-label" for="user_nickname">用户名</label>
						<div class="">
						<input class="form-control" id="user_nickname" name="username" size="50" type="text" value=""><span class="help-block red"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label" for="user_password">密码</label>
						<div class="">
						<input class="form-control" id="user_password" name="password" size="50" type="password" value="">
						<span class="help-block red"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label" for="captcha_code">验证码</label>
						<div class="">
						<input class="form-control" id="captcha_code" name="captcha_code" size="50" type="text" value="">
						<span class="help-block red"></span>
						</div>
						<div class="">
						<a href="javascript:reloadcode();" title="更换验证码"><img src="http://localhost/stbbs/index.php/captcha_code" name="checkCodeImg" id="checkCodeImg" border="0"></a>&nbsp;&nbsp;<a href="javascript:reloadcode();">换一张</a>
						</div>
					</div>
					<script language="javascript">
					//刷新图片
					function reloadcode() {//刷新验证码函数
					 var verify = document.getElementById('checkCodeImg');
					 verify.setAttribute('src', 'http://localhost/stbbs/index.php/captcha_code?' + Math.random());
					}
					</script>
					<div class="form-group">
						<div class="sub">
							<button class="btn btn-primary" id="btnLogin">登入</button>
							<a href="http://localhost/stbbs/index.php/user/findpwd" class="btn btn-default" role="button">找回密码</a>
						</div>
					</div>
				
                </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary">保存</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal-backdrop  in"></div>
<div id="cover" class="cover"></div>
<?php $this->load->view('common/footer');?>
</body>
</html>