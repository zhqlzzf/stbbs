<!DOCTYPE html>
<html>
<head>
<title><?php echo $content['title']?> - <?php echo $settings['site_name']?></title>
<meta charset='UTF-8'>
<meta content='True' name='HandheldFriendly'>
<meta content='width=device-width, initial-scale=1.0' name='viewport'>
<meta name="keywords" content="<?php echo '';?>" />
<meta name="description" content="<?php echo '';?>" />
<?php $this->load->view('common/xyheader-meta');?>
<script src="<?php echo base_url('static/xiaoyou/js/topic.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('static/common/js/plugins.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('static/common/js/jquery.upload.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('static/layer-v2.1/layer/layer.js')?>" type="text/javascript"></script>
<?php if($this->config->item('storage_set')=='local'){?>
<script src="<?php echo base_url('static/xiaoyou/js/local.file.js')?>" type="text/javascript"></script>
<?php } else{?>
<script src="<?php echo base_url('static/common/js/qiniu.js')?>" type="text/javascript"></script>
<?php }?>
</head>
<body >
<?php $this->load->view('common/xyheader'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading topic-detail-heading">
                        <div class="pull-right"><a href="<?php echo site_url('user/profile/'.$content['uid']);?>"><img src="<?php echo base_url($content['avatar'].'normal.png');?>" alt="<?php echo $content['username']?>';?>"></a></div>
                        <p><a href="<?php echo base_url();?>">首页</a> /</p>
                        <h2 class="panel-title"><?php echo $content['title']?></h2>
                        <small class="text-muted">
                            <span>By <a href="<?php echo site_url('user/profile/'.$content['uid']);?>"><?php echo $content['username']; ?></a></span>&nbsp;•&nbsp;
                            <span><?php echo date('Y-m-d H:i:s',$content['addtime']);?></span>&nbsp;•&nbsp;
                            
                        </small>
                    </div>
                    
                    <div class="panel-body content">
						<?php  ?>
                        <?php echo $content['content']?>
                    </div>
                   
                    <div class="panel-footer">
						<?php if($this->auth->is_user($content['uid']) || $this->auth->is_admin() || $this->auth->is_master($cate['node_id'])){?>
						
						<a href="<?php echo site_url('topic/edit/'.$content['topic_id']);?>" class="btn btn-default btn-sm unbookmark" data-method="edit" rel="nofollow">编辑</a>
						<?php }?>
                    </div>
                </div><!-- /.panel content -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h5><span> 直到<?php echo date('Y-m-d H:i',time()); ?>已经加入的同学</span></h5>
                    </div>
                    <div class="panel-body">
						<?php foreach((array)$InStudents as $sl) {?>
	  <?php echo $sl['username']?><?php }?>
					</div>
                </div><!-- /.panel comment -->
                <div id="error"></div>
            </div><!-- /.col-md-8 -->

			<div class="col-md-4">
			<?php $this->load->view('common/sidebar_login');?>
			<?php $this->load->view('common/sidebar_cateinfo');?>
			<!--<?php $this->load->view('common/sidebar_cates');?>-->
			<?php $this->load->view('common/sidebar_related_topic');?>
			<?php $this->load->view('common/sidebar_ad');?>
			</div><!-- /.col-md-4 -->

        </div><!-- /.row -->
    </div><!-- /.container -->
<div id="cover" class="cover"></div>
<?php $this->load->view('common/footer');?>
</body>
</html>