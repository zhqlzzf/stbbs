<!DOCTYPE html>
<html>
	<head>
<meta content='用户认领' name='description'>
<meta charset='UTF-8'>
<meta content='width=device-width, initial-scale=1.0' name='viewport'>
<title><?php echo $title?>- <?php echo $settings['site_name']?></title>
<?php $this->load->view('common/header-meta');?>
<script src="<?php echo base_url('static/common/js/claim.js')?>" type="text/javascript"></script>
</head>
<body id="startbbs">
<?php $this->load->view('common/header');?>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">用户认领</h3>
                    </div>
                    <div class="panel-body">
					<form accept-charset="UTF-8" action="<?php echo site_url('claim/doclaim');?>" class="form-horizontal" id="claimform" method="post" novalidate="novalidate">
					<input type="hidden" name="<?php echo $csrf_name;?>" value="<?php echo $csrf_token;?>">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="">所属院系</label>
						<div class="col-sm-5">
						<?php $js='id="depSelect"'; echo form_dropdown('depSelect',$yx,'',$js);?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="">入学年份</label>
						<div class="col-sm-5">
						<?php $js='id="yearSelect"'; echo form_dropdown('yearSelect',$year,'',$js);?>
						<span class="help-block red"><?php echo form_error('email');?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="">专业或班级</label>
						<div class="col-sm-5">
						<select id="classSelect" name="myClass"></select>
						<span class="help-block red"><?php echo form_error('xyclasses');?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="">真实姓名</label>
						<div class="col-sm-5">
						<input class="form-control" id="truename" name="truename"  value="" />
						</div>
						<span class="help-block red"><?php echo form_error('truename');?></span>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="user_password">密码</label>
						<div class="col-sm-5">
						<input class="form-control" id="user_password" name="password" type="password" value="<?php echo set_value('password'); ?>" />
						<span class="help-block red"><?php echo form_error('password');?></span>默认密码：111111
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="user_password_confirmation">密码确认</label>
						<div class="col-sm-5">
						<input class="form-control" id="user_password_confirmation" name="password_confirm" type="password" value="<?php echo set_value('password_confirm'); ?>" /><span class="help-block red"><?php echo form_error('password_confirm');?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="">手机号码</label>
						<div class="col-sm-5">
						<input class="form-control" id="phone" name="phone"  value="" />
						</div>
						<span class="help-block red"><?php echo form_error('phone');?></span>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="">邮箱</label>
						<div class="col-sm-5">
						<input class="form-control" id="email" name="email"  value="" />
						</div>
					</div>
					<div class='form-group'>
						<div class="col-sm-offset-2 col-sm-9">
							<button type="submit" name="claim" id="claim" class="btn btn-primary">认领</button><div id="msg" ></div>
						</div>
					</div>
					</form>
                    </div>
                </div>
            </div><!-- /.col-md-8 -->

            <div class="col-md-4">
				<?php $this->load->view('common/sidebar_login');?>
				<!--<?php $this->load->view('common/sidebar_ad');?>-->
            </div><!-- /.col-md-4 -->

        </div><!-- /.row -->
    </div><!-- /.container -->

<?php $this->load->view('common/footer');?>
</body>
</html>