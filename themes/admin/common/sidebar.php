            <div class="col-md-3">
                <div class="list-group">
                    <a href="" class="list-group-item disabled">管理面板</a>
				    <a href="<?php echo site_url('admin/site_settings');?>" class="list-group-item">基本设置</a>
					<a href="<?php echo site_url('admin/homepage_settings');?>" class="list-group-item">首页设置</a>
					<a href="<?php echo site_url('admin/xiaoyou_settings');?>" class="list-group-item">校友基础数据设置</a>
					<a href="<?php echo site_url('admin/users/index');?>" class="list-group-item">用户管理</a>	
					<a href="<?php echo site_url('admin/classes/index');?>" class="list-group-item">班级管理</a>				
					<a href="<?php echo site_url('admin/nodes');?>" class="list-group-item">栏目管理</a>
					<a href="<?php echo site_url('admin/topics');?>" class="list-group-item">文章管理</a>	
					<a href="<?php echo site_url('admin/topics/Zhufu_index');?>" class="list-group-item">祝福管理</a>					
					<a href="<?php echo site_url('admin/links');?>" class="list-group-item">链接管理</a>
					<a href="<?php echo site_url('admin/page');?>" class="list-group-item">单页面</a>
					<a href="<?php echo site_url('admin/import');?>" class="list-group-item">资料导入</a>
					<a href="<?php echo site_url('admin/db_admin/index');?>" class="list-group-item">数据库管理</a>
                </div>
            </div><!-- /.col-md-4 -->