<!DOCTYPE html><html><head><meta content='' name='description'>
<meta charset='UTF-8'>
<meta content='True' name='HandheldFriendly'>
<meta content='width=device-width, initial-scale=1.0' name='viewport'>
<title><?php echo $title?> - 管理后台 - <?php echo $settings['site_name']?></title>
<?php $this->load->view ('common/header-meta');?>
<script src="<?php echo base_url('static/common/js/node.js');?>" type="text/javascript"></script>
</head>
<body id="startbbs">
<?php $this->load->view ('common/header');?>
    <div class="container">
        <div class="row">
            <?php $this->load->view ('common/sidebar');?>
            <div class="col-md-9">
                <ol class="breadcrumb">
				  <li><a href="<?php echo site_url('admin/login')?>">管理首页</a></li>
				  <li class="active">结点列表</li>
				</ol>
				<div class="panel panel-default">
				  	<!--<div class="panel-heading">ttttt<small class='pull-right'>操作选项</small></div>-->
				  	<div class="panel-body">
					<?php if($cates){?>
					<table class="table table-hover table-condensed">
						<thead>
							<th>列表 (列表里序号>0的按顺序显示到首页导航)</th>
							<th><span class="pull-right">选项</span></th>
						</thead>
						<?php foreach($cates as $v){?>
						<tr>
							<td><a href="<?php echo site_url('node/show/'.$v['node_id']);?>" target="_blank" title="点击查看栏目下文章"><?php echo $v['cname'].' [node_id='.$v['node_id'].']';?></a></td>
							<td><span class="pull-right">
							<input type="text" id="sortid" style="width:20px;" value="<?php  echo $v['sortid']; ?>" /> 
							<a href="<?php echo site_url('/admin/nodes/sort/'.$v['node_id']).'/'.'up';?>" class="btn btn-info" data-remote="true" id="sort_node"  title="修改首页导航菜单显示位置">↑</a> 
							<a href="<?php echo site_url('/admin/nodes/sort/'.$v['node_id']).'/'.'down';?>" class="btn btn-info" data-remote="true" id="sort_node" title="修改首页导航菜单显示位置">↓</a> 
							<a href="<?php echo site_url('/admin/nodes/edit/'.$v['node_id']);?>" class="btn btn-primary btn-sm" data-remote="true" id="edit_node_1" title="修改栏目的属性">修改</a>
							<a href="<?php echo site_url('/admin/nodes/move/'.$v['node_id']);?>" class="btn btn-warning btn-sm" data-remote="true" title="移动栏目的上下级关系">移动</a>
							<a href="<?php echo site_url('/admin/nodes/del/'.$v['node_id']);?>" class="btn btn-sm btn-danger" onclick="delete_confirm();" rel="nofollow" title="删除这个栏目">删除</a>
							<a href="<?php echo site_url('/topic/add/'.$v['node_id']);?>" class="btn btn-primary btn-sm" data-remote="true" id="edit_node_1" title="在栏目下添加文章" target="_blank">添加文章</a></span>
							</td>
						</tr>
						<?php if($scates=$this->cate_m->get_cates_by_pid($v['node_id']))?>
						<?php foreach($scates as $s){?>
						<tr>
							<td>&nbsp;&nbsp;&nbsp;├─&nbsp;<a href="<?php echo site_url('node/show/'.$s['node_id']);?>" target="_blank" title="点击查看栏目下文章"><?php echo $s['cname'].' [node_id='.$s['node_id'].']';?></a></td>
							<td><span class="pull-right"><a href="<?php echo site_url('/admin/nodes/edit/'.$s['node_id']);?>" class="btn btn-primary btn-sm" data-remote="true" id="edit_node_1" title="修改栏目的属性">修改</a>
<a href="<?php echo site_url('/admin/nodes/move/'.$s['node_id']);?>" class="btn btn-warning btn-sm" data-remote="true" title="移动栏目的上下级关系">移动</a>
<a href="<?php echo site_url('/admin/nodes/del/'.$s['node_id']);?>" class="btn btn-sm btn-danger" onclick="delete_confirm();" rel="nofollow" title="删除这个栏目">删除</a><a href="<?php echo site_url('/topic/add/'.$s['node_id']);?>" target="_blank" class="btn btn-primary btn-sm" data-remote="true" id="edit_node_1" title="在栏目下添加文章" target="_blank">添加文章</a></span></td>
						</tr>
						<?php } ?>
						
						<?php } ?>
					</table>
					<?php } else {?>
					暂无分类
					<?php } ?>
					<a href="<?php echo site_url('admin/nodes/add');?>" class="btn btn-primary btn-sm" data-remote="true">添加节点</a>
					</div>

				</div>

            </div><!-- /.col-md-8 -->

        </div><!-- /.row -->
    </div><!-- /.container -->

<?php $this->load->view ('common/footer');?>
</body></html>
