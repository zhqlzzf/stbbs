<!DOCTYPE html>
<html>
<head>
<meta content='' name='description'>
<meta charset='UTF-8'>
<meta content='True' name='HandheldFriendly'>
<meta content='width=device-width, initial-scale=1.0' name='viewport'>
<title>资料导入 - 管理后台 -<?php echo $settings['site_name']?></title>
<?php $this->load->view ( 'common/header-meta' ); ?>
<script src="<?php echo base_url('static/common/js/plugins.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('static/common/js/jquery.upload.js')?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url('static/xiaoyou/js/importxls.js');?>"></script>
</head>
<body id="starbbs">
<?php $this->load->view ( 'common/header' ); ?>
<div class="container">
  <div class="row">
    <?php $this->load->view ( 'common/sidebar' ); ?>
    <div class="col-md-9">
      <div class="panel panel-default">
        <div class="panel-body">
			<ol class="breadcrumb">
				<li><a href="<?php echo site_url('admin/login')?>">管理首页</a></li>
				<li class="active">资料导入</li>
			</ol>
			<ul class="nav nav-tabs">
				<li class="active"><a href="#tab1" data-toggle="tab">导入专业、班级、学生信息</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane in active" id="tab1">					
					<form accept-charset="UTF-8"  class="form-horizontal" method="post" enctype="multipart/form-data">
						<input type="hidden" name="<?php echo $csrf_name; ?>" value="<?php echo $csrf_token; ?>" id="token">
						<fieldset>				    			
				    			<div class="form-group">
				      				<label class="col-md-2 control-label" for="xls_file">选择文件</label>
				      				<div class="col-md-6">
				       					<input type="file" id="xls_file" name="xls_file" />
				      				</div>
				    			</div>				    			
				    			<div class="form-group">
					    			<div class="col-sm-offset-2 col-sm-6">
				    				<button type="button" id="upload_file" class="btn btn-primary">1:上传文件</button>  
									<button type="button" id="importxls" class="btn btn-primary">2:数据校验和导入</button>
				    				</div>									
				    			</div>
				    	</fieldset>
						<div class="form-group">							
							<div class="col-sm-offset-1 col-sm-9">
								<div id="msg" style="color:red; "></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>

<?php $this->load->view ('common/footer');?>
<script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-tab.js"></script> 
<script type="text/javascript">
            $(function () {
                var log = function(s){
                    window.console && console.log(s)
                }
                $('.nav-tabs a:first').tab('show')
                $('a[data-toggle="tab"]').on('show', function (e) {
                    log(e)
                })
                $('a[data-toggle="tab"]').on('shown', function (e) {
                    log(e.target) // activated tab
                    log(e.relatedTarget) // previous tab
                })
            })

</script>
</body>
</html>