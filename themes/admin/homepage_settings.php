<!DOCTYPE html>
<html>
<head>
<meta content='' name='description'>
<meta charset='UTF-8'>
<meta content='True' name='HandheldFriendly'>
<meta content='width=device-width, initial-scale=1.0' name='viewport'>
<title>基本设置 - 管理后台 -<?php echo $settings['site_name']?></title>
<?php $this->load->view ( 'common/header-meta' ); ?>
</head>
<body id="starbbs">
<?php $this->load->view ( 'common/header' ); ?>
<div class="container">
  <div class="row">
    <?php $this->load->view ( 'common/sidebar' ); ?>
    <div class="col-md-9">
      <div class="panel panel-default">
        <div class="panel-body">
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin/login')?>">管理首页</a></li>
            <li class="active">前台首页设定</li>
          </ol>
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab1" data-toggle="tab">中部大图管理</a></li>
            <li><a href="#tab2" data-toggle="tab">底部图片设定</a></li>
          </ul>
          <div class="tab-content">
          <div role="tabpanel" class="tab-pane in active" id="tab1">
          <form accept-charset="UTF-8" action="<?php echo site_url('admin/homepage_settings?a=bigpic');?>" class="form-horizontal" method="post">
          <input type="hidden" name="<?php echo $csrf_name; ?>" value="<?php echo $csrf_token; ?>">
          <div class='form-group'>
							<label class="col-md-3 control-label" for="settings_site_name">首页显示大图数量</label>
							<div class='col-md-5'>
							<input id="settings_site_name" class="form-control" name="site_name" type="text" value="<?php echo '';?>" />

							</div>
							<small class='help-block'>必填</small>
							</div>
							<div class="form-group">
              <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" name="commit" class="btn btn-primary">保存</button>
              </div>
            </div>
          </form>
        </div>
        <div role="tabpanel" class="tab-pane" id="tab2">
          <form accept-charset="UTF-8" action="<?php echo site_url('admin/homepage_settings?a=bottompic');?>" class="form-horizontal" method="post">
            <input type="hidden" name="<?php echo $csrf_name; ?>" value="<?php echo $csrf_token; ?>">
			<div class='form-group'>
							<label class="col-md-3 control-label" for="settings_site_name">底部显示图片数量</label>
							<div class='col-md-5'>
							<input id="settings_site_name" class="form-control" name="site_name" type="text" value="<?php echo '';?>" />

							</div>
							<small class='help-block'>必填</small>
							</div>
            <div class="form-group">
              <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" name="commit" class="btn btn-primary">保存</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.col-md-8 -->
</div>
<!-- /.row -->
</div>
<!-- /.container -->

<?php $this->load->view ('common/footer');?>
<script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-tab.js"></script> 
<script type="text/javascript">
            $(function () {
                var log = function(s){
                    window.console && console.log(s)
                }
                $('.nav-tabs a:first').tab('show')
                $('a[data-toggle="tab"]').on('show', function (e) {
                    log(e)
                })
                $('a[data-toggle="tab"]').on('shown', function (e) {
                    log(e.target) // activated tab
                    log(e.relatedTarget) // previous tab
                })
            })

</script>
</body>
</html>