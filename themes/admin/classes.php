<!DOCTYPE html>
<html>
<head>
<meta content='' name='description'>
<meta charset='UTF-8'>
<meta content='True' name='HandheldFriendly'>
<meta content='width=device-width, initial-scale=1.0' name='viewport'>
<title><?php echo $title?> - 管理后台 - <?php echo $settings['site_name']?></title>
<?php $this->load->view ( 'common/header-meta' ); ?>
</head>
<body id="startbbs">
<?php $this->load->view ('common/header'); ?>

    <div class="container">
        <div class="row">
            <?php $this->load->view ('common/sidebar');?>
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
	                    <ol class="breadcrumb">
						  <li><a href="<?php echo site_url('admin/login')?>">管理首页</a></li>
						  <li class="active">班级列表</li><a href="<?php echo site_url('topic/add/');?>" target="_blank"> 暂时点击这里上传班级照片</a>						</ol>
						<ul class="nav nav-pills">
						    <li<?php if($act=='index' || $act=='search'){?> class="active"<?php }?>><a href="<?php echo site_url('admin/classes/index');?>">班级列表</a></li>
					    </ul>
						<?php if($act=='index' || $act=='search'){?>
							<table class='table table-hover table-condensed'>
								<thead>
									<tr>
										<th>ID</th>
										<th>班级名称</th>
										<th>院系</th>
										<th>入学年份</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($classes as $v){?>
									<tr id='user_<?php echo $v['c_id']?>'>
										<td>
											<?php echo $v['c_id']?>
										</td>	
										<td>
											<strong><a href="<?php echo site_url('topic/show/'.$v['c_id']);?>" class="black startbbs profile_link" title="admin" target="_blank"><?php echo $v['c_name']?></a></strong>
										</td>
										<td>	<strong class='green'><?php echo $v['dep_id']?></strong>
										</td>
										<td>
											<?php echo $v['inschoolyear']?>
										</td>
										
										<td class='center'>	
										<a href="<?php echo site_url('classes/show/'.$v['c_id']);?>" target="_blank" class="btn btn-primary btn-sm">管理</a>
										</td>
									</tr>
									<?php }?>
								</tbody>
							</table>
						
							<?php if(@$pagination){?>
								<ul class='pagination'>
								<?php if($act=='index') echo $pagination?>
								</ul>
							<?php }?>
							<ul class='pagination'>
							<?php echo form_open('admin/classes/search', array('class'=>'form-inline'));?>
								<input id="username" class="form-control" name="username" placeholder="班级名称" type="text" />
								<button type="submit" name="commit" class="btn btn-default">搜索</button>
							</form>
							</ul>
						<?php }?>
                    </div>
                </div>
            </div><!-- /.col-md-8 -->

        </div><!-- /.row -->
    </div><!-- /.container -->

<?php $this->load->view ( 'common/footer' ); ?>
</body></html>