(function($) {
	/**
	 * 全选反选
	 */
	$.fn.check =function(obj){ 
		$('.checkboxes').each(function(){
			if(obj.checked){
				$(this).prop('checked',true);
			}else
				$(this).prop('checked',false);
			$(this).click(function () {
                if ($(this).attr("checked")) {
                } else {
                    $("#scanId").attr("checked", false);
                }
            })
		});
	}
    /**
    * 选择列查看图表
    */
	$.fn.viewStatictisc = function (obj) {
           var count = 0;
           $(".checkboxes_column").each(function () {
               if (this.checked) {
               	count++;
               }
           })
           if (count == 0) {
               alert("请至少选中一项");
               return false;
           }
          return true; 
      }	
	/**
	* 修改
	*/	
	$.fn.update = function (message) {
                var count = 0;
                var mes;
                $('.checkboxes').each(function(){
                    if (this.checked){
                    	count++;
                    }
                });
                if (count == 0){
                	mes=message==null?'请选中一项':message;
                    alert(mes);
                    return false;
                }
                if(count>1){
                	mes=message==null?'只能选一行':message;
                	alert(mes);
                	return false;
                }
                return true;
            }
    /**
    * 删除
    */
	$.fn.delet = function (obj) {
                var count = 0;
                $(".checkboxes").each(function () {
                    if (this.checked) {
                    	count++;
                    }
                })
                if (count == 0) {
                    alert("请至少选中一项");
                    return false;
                }
                if(confirm('确定要删除吗？')){
                	return true;
                }else
                	return false;
               return true; 
            }
     
	/**
	    * 自动分配房间和床位 至少选中一项
	    */
		$.fn.auto = function (obj) {
	                var count = 0;
	                $(".checkboxes").each(function () {
	                    if (this.checked) {
	                    	count++;
	                    }
	                })
	                if (count == 0) {
	                    alert("请至少选中一项");
	                    return false;
	                }
	               return true; 
	            }
		
	//必需是正整数 by wyl
	$.fn.matchInt=function(str){
		var reg = /[1-9]+\d*$/;
		return reg.test(str);
	}
	//至少两位数字
	$.fn.matchNum=function(str){
		var reg = /^\d{2,11}$/;
		return reg.test(str);
	}
	
	//必需是整数
	$.fn.isInt=function(str){
		 var reg = /^[1-9]\d?$/;
		  return reg.test(str);
	}

	 //必需是小数
	$.fn.isFloat=function(str){
			  var reg =/^\d+\.\d+$/;
			  return reg.test(str);
	}
		//激活指定标签
	$.fn.active=function(obj){
		var id=$(obj).val();
		if(obj.checked){
			$('.actives_'+id).attr("disabled",false);
		}else{
			$('.actives_'+id).attr("checked",false);
			$('.actives_'+id).attr("disabled",true);	
		}
	}
	
	
	/**
	* 判断日期是否大于当前日期
	*/
	$.fn.checkCurrent = function(obj){
		var currDate;
		if(obj.length>10){
			currDate = new Date().format("yyyy-MM-dd hh:mm:ss");
		}else{
			currDate = new Date().format("yyyy-MM-dd");
		}
		
		obj =  new Date(obj.replace("-", "/").replace("-", "/"));
		currDate = new Date(currDate.replace("-", "/").replace("-", "/"));
		if(obj - currDate < 0){
			alert('日期必须大于等于当前日期当前时间');
			return true;
		}else{
			return false;
		}
	}
	
	/**
	* 判断开始日期是否大于结束日期时间
	*/
	$.fn.isBeginOverEnd = function(begin,end,message){
		begin = new Date(begin.replace("-", "/").replace("-", "/"));
		end = new Date(end.replace("-", "/").replace("-", "/"));
		if(begin - end >=0){
			message=message==null?'结束日期必须大于开始日期':message;
			alert(message);
			return true;
		}else{
			return false;
		}
	}
	/**
	* 判断日期时间比较
	* begin、end为日期类型
	* 
	*/
	$.fn.dateCompare = function(begin,end){
		if(Date.parse(end)<=Date.parse(begin)){
			alert('结束日期必须大于开始日期');
			return true;
		}else{
			return false;
		}
	}
	
	$.fn.checkTime = function(begin,end,model){
		begin = new Date(begin.replace("-", "/").replace("-", "/"));
		end = new Date(end.replace("-", "/").replace("-", "/"));
		if(begin - end >=0){
			if(model == "rw"){
				alert('退伍时间必须大于入伍时间');
				return true;
			}
		}else{
			return false;
		}
	}
	/**
	* 时间对象的格式化
	*/ 
	Date.prototype.format = function(format) { 
		var o = { 
		"M+" : this.getMonth() + 1, 
		"d+" : this.getDate(), 
		"h+" : this.getHours(), 
		"m+" : this.getMinutes(), 
		"s+" : this.getSeconds(), 
		"q+" : Math.floor((this.getMonth() + 3) / 3), 
		"S" : this.getMilliseconds() 
		} 
		 
		if (/(y+)/.test(format)) 
		{ 
		format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 
		- RegExp.$1.length)); 
		} 
		 
		for (var k in o) 
		{ 
		if (new RegExp("(" + k + ")").test(format)) 
		{ 
		format = format.replace(RegExp.$1, RegExp.$1.length == 1 
		? o[k] 
		: ("00" + o[k]).substr(("" + o[k]).length)); 
		} 
		} 
		return format; 
	} 
	/**
	 * 验证输入为数字类型或为空
	 */
	$.fn.checkNumOrDouble=function(obj,flag){
		var res=$(obj).val();
		var pattern;
		var message;
		if(res=='')return true;
		if(flag){//Int
			pattern=/^[1-9]*$/;
			message="输入项必须为正整数";
		}else{
			pattern=/^\d+(\.\d{1,2})?$/;
			message="输入项为浮点数且精确度最大为2位";
		}
		if(!pattern.test(res)){
			$(obj).val('');
			$(obj).focus();//IE
			window.setTimeout(function(){ $('#'+obj.id).focus();}, 0);//Firefox
			alert(message);
			
			return false;
		}
		return true;
	}
	/**
	*显示加载信息
	*/
	$.fn.loading = function(obj){
		if($(obj).val() == '登录'){
			$('#logining').hide();
		 	$("#loading").text("正在登录…");
	 	}else{
	 		$('.loadingspinner').html("<span style='float:left'>正在加载...</span>").show();
	 	}
	}
	$.fn.registor = function(obj){
		if($(obj).val() == '注册'){
			$('.loadlogin').hide();
			$('#loadlogin').show();
		 	$("#loadlogin").text("请稍候…");
	 	}else{
	 		$('.loadingspinner').html("<span style='float:left'>请稍候...</span>").show();
	 	}
	}
	/**
	*获得录取学生普通where条件
	*/
	$.fn.lqxsgeneralWhere = function(tableName){//st_bys_info
		$("#generalWhere").val("");
		var mzId = $("#mzId").val();
		var zyId = $("#zyId").val();
		var sydId = $("#sydId").val();
		var nf = $("#nf").val();
		var xb = $("#xb").val();
		//页面直接拼接语句，免得后台根据传参个数重新拼接
		if((mzId != "" && mzId != null) || (zyId != "" && zyId != null) 
			|| (sydId != "" && sydId != null) || (nf != "" && nf != null) 
			|| (xb != "" && xb != null) ){
			
			var sql = "";
			if(tableName == "Student"){//若为毕业生表
				sql = "select id from "+tableName+" s where 1=1";
			}else{
				sql = "select id from "+tableName+" , Student s where xjb_id = xh";
			}
			if(mzId != "" && mzId != null){
				sql += " and s.mzb.id = "+mzId;
			}
			if(zyId != "" && zyId != null){
				sql += " and s.zyb.id = "+zyId;
			}
			if(sydId != "" && sydId != null){
				sql += " and s.syd.id = "+sydId;
			}
			if(nf != "" && nf != null){
				sql += " and s.nf like  '%"+nf+"%'";
			}
			if(xb != "" && xb != null){
				sql += " and s.xb like  '%"+xb+"%'";
			}
			$("#generalWhere").val(sql);
		}	
		return sql;
	}
	/**
	*获得普通where条件
	*/
	$.fn.generalWhere = function(tableName){
		$("#generalWhere").val("");
		var username = $("#username").val();
		var xm = $("#xm").val();
		var sfzh = $("#sfzh").val();
		var dwId = $("#dwid").val();
		var zyId = $("#zybid").val();
		var bjId = $("#bjid").val();
		var rxrq = $("#rxrq").val();
		var byrq = $("#byrq").val();
		var sfzdxy = $("#sfzdxy").val();
		var xb = $("#xb").val();
		//页面直接拼接语句，免得后台根据传参个数重新拼接
		if((dwId != "" && dwId != null) || (zyId != "" && zyId != null) 
			|| (bjId != "" && bjId != null) || (username != "" && username != null) 
			|| (xm != "" && xm != null) || (sfzh != "" && sfzh != null) || (rxrq != "" && rxrq != null) 
			|| (byrq != "" && byrq != null) || (sfzdxy != "" && sfzdxy != null)){
			var sql = "";
			if(dwId != "" && dwId != null){
				sql += " and xs_xjb.dwb_id = "+dwId;
			}
			if(zyId != "" && zyId != null){
				sql += " and xs_xjb.zyb_id = "+zyId;
			}
			if(bjId != "" && bjId != null){
				sql += " and xs_xjb.bjb_id = "+bjId;
			}
			if(username != "" && username != null){
				sql += " and xs_xjb.username like  '%"+username+"%'";
			}
			if(xm != "" && xm != null){
				sql += " and xs_xjb.xm like  '%"+xm+"%'";
			}
			if(sfzh != "" && sfzh != null){
				sql += " and "+tableName+".sfzh = '"+sfzh+"'";
			}
			if(rxrq != "" && rxrq != null){
				sql += " and "+tableName+".rxrq = '"+rxrq+"'";
			}
			if(byrq != "" && byrq != null){
				sql += " and "+tableName+".byrq = '"+byrq+"'";
			}
			if(sfzdxy != "" && sfzdxy != null){
				sql += " and "+tableName+".sfzdxy = '"+sfzdxy+"'";
			}
			if(xb != "" && xb != null){
				sql += " and "+tableName+".xb = '"+xb+"'";
			}
			$("#generalWhere").val(sql);
		}	
		return sql;
	}
	/**
	*奖、助、贷、贫困生、特困补助、学生基本信息等选择多列统计分析
	*/
	$.fn.multipleStatistics = function(tableName){
			$("#viewStatistics-pies,#viewStatistics-columns,#viewStatistics-lines").click(function(){
			if($.fn.viewStatictisc()){
				$("#chartType").val(this.id);
				var action = $("#advancedSearch").attr("formAction");
				$("#action").val(action);
				$.fn.generalWhere(tableName);	
				$("#dataManageForm").attr("action",action+"/multipleStatistics").submit();
			}
		});	
	}
	/**
	*鼠标移至饼图上，显示其他2种图表
	*/
	$.fn.hoverChartImage = function(){
		var flag=false;
		$(".column").hide();
		$(".line").hide();
		$(".chartimage").each(function(){
			var widthcol=$(this).width();
			if(widthcol>80){
				widthcol=widthcol+50+"px";
				$(this).css("min-width",widthcol)/*.css("width",widthcol)*/
				$(this).addClass("chartimagespecial");
			}
		});
		$(".chartimage").hover(function(){
			flag=$(this).hasClass("chartimagespecial");
			if(flag){
				var widthcontrol=$(this).width()+51+"px";
				$(this).css("min-width",widthcontrol)/*.css("width",widthcontrol);*/
			}
			$(this).children(".column").show();
			$(this).children(".line").show();
		},function(){
			flag=$(this).hasClass("chartimagespecial");
			$(this).children(".column").hide();
			$(this).children(".line").hide();
			if(flag){
				var widthoutcontrol=$(this).width()-51+"px";
				$(this).css("min-width",widthoutcontrol)/*.css("width",widthoutcontrol);*/
			}
		});	
	}
	
	})(jQuery);
